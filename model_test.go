package pjrpc_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/storage"
)

type dataType struct {
	Data string
}

func (dt dataType) MarshalJSON() ([]byte, error) {
	return []byte(`"` + dt.Data + `"`), nil
}

// TestErrorString testing of the error string formatter.
func TestErrorString(t *testing.T) {
	t.Parallel()

	e := &pjrpc.ErrorResponse{
		Code:    1234,
		Message: "Custom Error",
		Data:    nil,
	}

	assert := func(want string) {
		got := e.Error()
		if want != got {
			t.Fatalf("wrong result:\n%s\n%s", want, got)
		}
	}

	assert("JSON-RPC Error: [1234] Custom Error")

	e.Data = json.RawMessage(`"Some text"`)
	assert(`JSON-RPC Error: [1234] Custom Error ("Some text")`)

	structData := struct {
		Data string
	}{
		Data: "Some text",
	}

	var err error
	if e.Data, err = json.Marshal(structData); err != nil {
		t.Fatal(err)
	}

	assert(`JSON-RPC Error: [1234] Custom Error ({"Data":"Some text"})`)

	if e.Data, err = json.Marshal(dataType{Data: "Some text"}); err != nil {
		t.Fatal(err)
	}

	assert(`JSON-RPC Error: [1234] Custom Error ("Some text")`)

	e = nil
	assert("<nil>")
}

func TestRequestID(t *testing.T) {
	t.Parallel()

	testCase := func(body string, wantID string) {
		t.Helper()

		req := new(pjrpc.Request)

		if err := json.Unmarshal([]byte(body), req); err != nil {
			t.Fatal("failed json.Unmarshal:", err)
		}

		if req.JSONRPC != pjrpc.JSONRPCVersion {
			t.Fatal("wrong version:", req.JSONRPC)
		}

		if req.Method != "method" {
			t.Fatal("not 'method':", req.Method)
		}

		if req.Params == nil {
			t.Fatal("params is nil")
		}

		gotID := req.GetID()
		if wantID != gotID {
			t.Fatal("wrong id:", gotID)
		}
	}

	testCase(`{"jsonrpc":"2.0","method":"method","params":{},"id":123}`, "123")
	testCase(`{"jsonrpc":"2.0","method":"method","params":{},"id":"123"}`, "123")
	testCase(`{"jsonrpc":"2.0","method":"method","params":{},"id":""}`, "")
	testCase(`{"jsonrpc":"2.0","method":"method","params":{}}`, "")
}

func TestResponseID(t *testing.T) {
	t.Parallel()

	testCase := func(wantID string, wantBody string) {
		t.Helper()

		jsonID, err := json.Marshal(wantID)
		if err != nil {
			t.Fatal("failed to json.Marshal id:", err)
		}

		if wantID == "" {
			jsonID = nil
		}

		resp := &pjrpc.Response{
			JSONRPC: pjrpc.JSONRPCVersion,
			ID:      jsonID,
		}

		body, err := json.Marshal(resp)
		if err != nil {
			t.Fatal("failed to json.Marshal resp:", err)
		}

		if wantBody != string(body) {
			t.Fatalf("wrong body: %q", body)
		}

		gotID := resp.GetID()
		if wantID != gotID {
			t.Fatalf("wrong id: %q", gotID)
		}
	}

	testCase("1", `{"jsonrpc":"2.0","id":"1"}`)
	testCase("some-text", `{"jsonrpc":"2.0","id":"some-text"}`)
	testCase("", `{"jsonrpc":"2.0","id":null}`)
}

func TestResponse_SetError(t *testing.T) {
	t.Parallel()

	testCase := func(err error, want string) {
		t.Helper()

		resp := &pjrpc.Response{Result: []byte("some bytes")}
		resp.SetError(err)

		if resp.Result != nil {
			t.Fatal("result not empty: ", string(resp.Result))
		}

		got, err := json.Marshal(resp)
		if err != nil {
			t.Fatal("json.Marshal: ", err)
		}

		if want != string(got) {
			t.Fatalf("wrong result:\n%s\n%s", want, got)
		}
	}

	testCase(
		pjrpc.JRPCErrInvalidParams("field_name"),
		`{"jsonrpc":"","id":null,"error":{"code":-32602,"message":"Invalid params","data":"field_name"}}`,
	)

	testCase(
		storage.ErrRouteNotFound,
		`{"jsonrpc":"","id":null,"error":{"code":-32601,"message":"Method not found"}}`,
	)

	testCase(
		pjrpc.ErrBadStatusCode,
		`{"jsonrpc":"","id":null,"error":{"code":-32000,"message":"Server error","data":"bad status code"}}`,
	)
}

func TestResponse_SetResult(t *testing.T) {
	t.Parallel()

	testCase := func(result interface{}, want string) {
		t.Helper()

		resp := new(pjrpc.Response)
		resp.SetResult(result)

		got, err := json.Marshal(resp)
		if err != nil {
			t.Fatal("json.Marshal: ", err)
		}

		if want != string(got) {
			t.Fatalf("wrong result:\n%s\n%s", want, got)
		}
	}

	testCase(
		"some data",
		`{"jsonrpc":"","id":null,"result":"some data"}`,
	)

	testCase(
		pjrpc.JRPCErrInternalError(),
		`{"jsonrpc":"","id":null,"result":{"code":-32603,"message":"Internal error"}}`,
	)

	testCase(
		make(chan int),
		`{"jsonrpc":"","id":null,"error":{"code":-32603,"message":"Internal error","data":"failed to marshal response"}}`,
	)
}
