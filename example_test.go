package pjrpc_test

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"

	"gitlab.com/pjrpc/pjrpc/v2"
)

type rpc struct{}

type request struct {
	Name string `json:"name"`
}

type response struct {
	Name string `json:"name"`
}

func middleware(next pjrpc.Handler) pjrpc.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		fmt.Println("I'm middleware :)")
		return next(ctx, params)
	}
}

func (*rpc) helloHandler(ctx context.Context, params json.RawMessage) (interface{}, error) {
	req := &request{}
	err := json.Unmarshal(params, req)
	if err != nil {
		return nil, pjrpc.JRPCErrInvalidParams(err.Error())
	}

	fmt.Println("request from:", req.Name)
	return &response{Name: "server"}, nil
}

func ExampleServerHTTP() {
	r := &rpc{}
	srv := pjrpc.NewServerHTTP()

	// add your error logger to the server to catch error in write response method and panics.
	srv.SetLogger(log.Writer())

	srv.With(middleware)

	srv.RegisterMethod("hello", r.helloHandler)

	mux := http.NewServeMux()
	mux.Handle("/rpc", srv)

	// In real life you will start listener of the http server...

	rec := httptest.NewRecorder()
	bodyReq := strings.NewReader(`{"jsonrpc":"2.0","id":"1","method":"hello","params":{"name":"client"}}`)
	req := httptest.NewRequest(http.MethodPost, "/rpc", bodyReq)
	req.Header.Set("Content-Type", "application/json")

	mux.ServeHTTP(rec, req)

	fmt.Println("=== text after request ===")
	fmt.Println(rec.Code)
	fmt.Println(rec.Body.String())

	// Output:
	// I'm middleware :)
	// request from: client
	// === text after request ===
	// 200
	// {"jsonrpc":"2.0","id":"1","result":{"name":"server"}}
}
