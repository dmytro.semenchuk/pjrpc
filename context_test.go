package pjrpc_test

import (
	"context"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
)

func TestContext(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	data, ok := pjrpc.ContextGetData(ctx)
	if ok {
		t.Fatal("must be empty")
	}

	if data != nil {
		t.Fatal("must be empty")
	}

	data = &pjrpc.ContextData{}

	ctx = pjrpc.ContextSetData(ctx, data)

	got, ok := pjrpc.ContextGetData(ctx)
	if !ok {
		t.Fatal("must be not empty")
	}

	if data != got {
		t.Fatal("must be equal")
	}
}
