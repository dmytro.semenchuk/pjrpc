package main

import (
	"context"
	"log"

	"gitlab.com/pjrpc/pjrpc/v2/client"

	"helloworld/model/hello/rpcclient"
	"helloworld/model/types"
)

func main() {
	cl, err := client.New("http://127.0.0.1:8080/rpc/")
	if err != nil {
		log.Fatalf("failed to client.New: %s", err)
	}
	cl.SetLogger(log.Writer()) // Client can write errors about closing the response body.

	rpc := rpcclient.NewHelloServiceClient(cl)

	resp, err := rpc.HelloMethod(context.Background(), &types.HelloRequest{Name: "Client", Option: types.EnumFirst})
	if err != nil {
		log.Fatalf("got client error: %s", err)
	}

	log.Println("rpc response with:", resp.Name)
}
