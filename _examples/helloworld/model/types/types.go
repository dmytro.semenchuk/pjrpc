// Package types contains transport types of the JSON-RPC service.
package types

// ErrorData used like rpc field error.data in response with error.
// It will be showed in openapi spec if you passed it in service description.
type ErrorData struct {
	ClientMessage string `json:"client_message"`
}

// Enum is a list of the constants.
type Enum string

const (
	EnumFirst  Enum = "FIRST"  // Just first value.
	EnumSecond Enum = "SECOND" // Another value.
	EnumThird  Enum = "THIRD"  // Third value.
)

// HelloRequest fields of the Hello request.
// JSON tag (omitempty) and genpjrpc is an important part of the openapi spec.
type HelloRequest struct {
	Name   string `json:"name"`
	Option Enum   `json:"enum,omitempty" genpjrpc:"deprecated:true"` // Comments will be printed in the spec.
}

// HelloResponse fields of the Hello response.
// Also you can use base Go type or slices
// as a request or a response, BUT it must be a named type.
//   type HelloResponse string.
type HelloResponse struct {
	Name string `json:"name"`
}
