// Package hello contains the rpc service description just interface with methods.
package hello

import (
	"helloworld/model/types"
)

// HelloService you can describe your service with methods.
//go:generate genpjrpc -search.name=HelloService -print.place.path_swagger_file=../../api/swagger.json -print.content.swagger_data_path=./swagger_data.json
type HelloService interface {
	// You can set your own 'error'.'data' type to add some additional error context.
	setErrorData(types.ErrorData)

	// HelloMethod documentation of the Hello RPC method.
	// Name of the method will convert to 'hello_method' (snake_case) JSON-RPC method.
	// You must use named Go type as request and response of the method.
	// Also you can use special magic comment to pass some settings for swagger spec.
	// See README for details: //genpjrpc:params.
	HelloMethod(types.HelloRequest) types.HelloResponse
}
