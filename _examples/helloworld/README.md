genpjrpc
========

# Run

To generate code just run:

```
go generate ./...
```

(genpjrpc must be installed)

Then run server:

```
go run ./cmd/server/
```

And then run client:

```
go run ./cmd/client/
```

# Description

- `model/types` - transport models of the JSON RPC service.
- `model/hello/service.go` - service description with methods and go:generate comment.
- `model/hello/swagger_data.json` - pre filled openapi specs for generator.
- `model/hello/rpcclient` / `model/hello/rpcserver` - generated files for Go client and server of the RPC service.
- `api/swagger.json` - generated openapi spec with methods and types.
- `cmd` - client and server bins.
