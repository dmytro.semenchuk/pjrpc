package main

import (
	"context"
	_ "embed"
	"log"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/v2"

	"wasm/service"
	"wasm/service/rpcserver"
)

var (
	//go:embed static/client.wasm
	clientWasm []byte
	//go:embed static/index.html
	indexHTML []byte
	//go:embed static/wasm_exec.js
	wasmExec []byte
	//go:embed static/favicon.ico
	favicon []byte
)

type rpcService struct{}

func (r *rpcService) Hello(ctx context.Context, in service.In) (service.Out, error) {
	log.Println("got request: ", in)
	return "Server", nil
}

func serveFile(file []byte) http.HandlerFunc {
	return func(w http.ResponseWriter, _ *http.Request) { w.Write(file) }
}

func main() {
	srv := pjrpc.NewServerHTTP()
	srv.SetLogger(log.Writer()) // Server can write body close errors and panics in handlers.

	svc := &rpcService{}

	rpcserver.RegisterServiceServer(srv, svc)

	mux := http.NewServeMux()

	mux.Handle("/", serveFile(indexHTML))
	mux.Handle("/favicon.ico", serveFile(favicon))
	mux.Handle("/wasm_exec.js", serveFile(wasmExec))
	mux.Handle("/client.wasm", serveFile(clientWasm))
	mux.Handle("/rpc/", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("http.ListenAndServe: %s", err)
	}
}
