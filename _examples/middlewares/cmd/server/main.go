package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"net/http/httputil"
	"time"

	"gitlab.com/pjrpc/pjrpc/v2"

	"middlewares/service"
	"middlewares/service/rpcserver"
)

type ctxKey int

const ctxKeyLogger ctxKey = iota + 1

type rpcService struct{}

// timer should be called last before method handler.
func (rpcService) timer(next pjrpc.Handler) pjrpc.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		now := time.Now()

		res, err := next(ctx, params)
		dur := time.Since(now).Seconds()

		log := ctx.Value(ctxKeyLogger).(*log.Logger)
		log.Printf("execute method %.6f\n", dur)

		return res, err
	}
}

// contextLogger should be called first to add logger in the context.
func (rpcService) contextLogger(next pjrpc.Handler) pjrpc.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		log := log.New(log.Writer(), "[ctx-logger] ", log.LstdFlags)
		ctx = context.WithValue(ctx, ctxKeyLogger, log)

		log.Println("I added logger to the context :)")

		return next(ctx, params)
	}
}

// requestDebugger just debugger called after logger.
func (rpcService) requestDebugger(next pjrpc.Handler) pjrpc.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		data, _ := pjrpc.ContextGetData(ctx)

		dump, err := httputil.DumpRequest(data.HTTTRequest, false)
		if err != nil {
			ctx.Value(ctxKeyLogger).(*log.Logger).Printf("failed to DumpRequest: %s\n", err)
			return next(ctx, params)
		}

		log.Println("request:", string(dump))
		return next(ctx, params)
	}
}

func (rpcService) exclusiveMiddleware(next pjrpc.Handler) pjrpc.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		data, _ := pjrpc.ContextGetData(ctx)

		if data.JRPCRequest.Method != rpcserver.JSONRPCMethodExclusive {
			panic("wrong method: " + data.JRPCRequest.Method)
		}

		log.Println("exclusive middleware")

		return next(ctx, params)
	}
}

func (rpcService) Method(ctx context.Context, in *service.In) (service.Out, error) {
	log.Println("handler Method:", in.Field)

	return service.Out(in.Field), nil
}

func (rpcService) Exclusive(ctx context.Context, in *service.In) (service.Out, error) {
	log.Println("handler Exclusive:", in.Field)

	return service.Out(in.Field), nil
}

func main() {
	srv := pjrpc.NewServerHTTP()
	srv.SetLogger(log.Writer()) // Server can write body close errors and panics in handlers.

	svc := rpcService{}

	// Register srvice with common middlewares.
	// Middlewares will be called from left to right.
	rpcserver.RegisterServiceServer(srv, svc, svc.contextLogger, svc.requestDebugger, svc.timer)

	srv.Router.MethodWith(rpcserver.JSONRPCMethodExclusive, svc.exclusiveMiddleware)

	mux := http.NewServeMux()
	mux.Handle("/rpc/", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("http.ListenAndServe: %s", err)
	}
}
