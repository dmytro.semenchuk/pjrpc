package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/pjrpc/pjrpc/v2"
)

type rpc struct{}

type request struct {
	Name string `json:"name"`
}

type response struct {
	Name string `json:"name"`
}

func middleware(next pjrpc.Handler) pjrpc.Handler {
	return func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		log.Println("I'm middleware :)")
		return next(ctx, params)
	}
}

func (*rpc) helloHandler(ctx context.Context, params json.RawMessage) (interface{}, error) {
	req := &request{}
	err := json.Unmarshal(params, req)
	if err != nil {
		return nil, pjrpc.JRPCErrInvalidParams(err.Error())
	}

	log.Println("request from:", req.Name)

	return &response{Name: "server"}, nil
}

func main() {
	r := &rpc{}

	srv := pjrpc.NewServerHTTP()
	srv.SetLogger(log.Writer()) // Server can write body close errors and panics in handlers.
	srv.With(middleware)
	srv.RegisterMethod("hello", r.helloHandler)

	mux := http.NewServeMux()
	mux.Handle("/rpc", srv)

	log.Println("Starting rpc server on :8080")

	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("http.ListenAndServe: %s", err)
	}
}
