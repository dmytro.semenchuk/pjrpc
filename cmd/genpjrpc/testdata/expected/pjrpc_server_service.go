// Code generated by genpjrpc. DO NOT EDIT.
//  genpjrpc version: v0.2.0
//go:build testfiles

package service

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/testdata/service"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/testdata/service/common"
	pjrpc "gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/pjson"
)

// List of the server JSON-RPC methods.
const (
	JSONRPCMethodSpecialMethod       = "special_method_name"
	JSONRPCMethodDeprecatedMethod    = "deprecated_method"
	JSONRPCMethodSlices              = "slices"
	JSONRPCMethodBasic               = "basic"
	JSONRPCMethodMap                 = "map"
	JSONRPCMethodFromExternalPackage = "from_external_package"
	JSONRPCMethodSecondaryUseType    = "secondary_use_type"
)

// ServiceServer is an API server for Service service.
type ServiceServer interface {
	// Method comments.
	SpecialMethod(ctx context.Context, in *service.In) (*service.Out, error)

	// DeprecatedMethod method with mark deprecated.
	//
	// Deprecated: Do not use.
	DeprecatedMethod(ctx context.Context, in *service.In) (*service.Out, error)
	Slices(ctx context.Context, in service.SliceInt) (service.SliceStruct, error)
	Basic(ctx context.Context, in service.AliasInt) (service.AliasString, error)
	Map(ctx context.Context, in service.Dictionary) (*service.Out, error)
	FromExternalPackage(ctx context.Context, in *common.Type) (*common.EmptyType, error)
	SecondaryUseType(ctx context.Context, in *service.ReqType) (*service.RespType, error)
}

type regService struct {
	svc ServiceServer
}

// RegisterServiceServer registers rpc handlers with middlewares in the server router.
func RegisterServiceServer(srv pjrpc.Registrator, svc ServiceServer, middlewares ...pjrpc.Middleware) {
	r := &regService{svc: svc}

	srv.RegisterMethod(JSONRPCMethodSpecialMethod, r.regSpecialMethod)
	srv.RegisterMethod(JSONRPCMethodDeprecatedMethod, r.regDeprecatedMethod)
	srv.RegisterMethod(JSONRPCMethodSlices, r.regSlices)
	srv.RegisterMethod(JSONRPCMethodBasic, r.regBasic)
	srv.RegisterMethod(JSONRPCMethodMap, r.regMap)
	srv.RegisterMethod(JSONRPCMethodFromExternalPackage, r.regFromExternalPackage)
	srv.RegisterMethod(JSONRPCMethodSecondaryUseType, r.regSecondaryUseType)

	srv.With(middlewares...)
}

func (r *regService) regSpecialMethod(ctx context.Context, params json.RawMessage) (interface{}, error) {
	in := new(service.In)
	if params != nil {
		if err := pjson.Unmarshal(params, in); err != nil {
			return nil, pjrpc.JRPCErrParseError("failed to parse params")
		}
	}

	res, err := r.svc.SpecialMethod(ctx, in)
	if err != nil {
		return nil, fmt.Errorf("failed SpecialMethod: %w", err)
	}

	return res, nil
}

func (r *regService) regDeprecatedMethod(ctx context.Context, params json.RawMessage) (interface{}, error) {
	in := new(service.In)
	if params != nil {
		if err := pjson.Unmarshal(params, in); err != nil {
			return nil, pjrpc.JRPCErrParseError("failed to parse params")
		}
	}

	res, err := r.svc.DeprecatedMethod(ctx, in)
	if err != nil {
		return nil, fmt.Errorf("failed DeprecatedMethod: %w", err)
	}

	return res, nil
}

func (r *regService) regSlices(ctx context.Context, params json.RawMessage) (interface{}, error) {
	var in service.SliceInt
	if params != nil {
		if err := pjson.Unmarshal(params, &in); err != nil {
			return nil, pjrpc.JRPCErrParseError("failed to parse params")
		}
	}

	res, err := r.svc.Slices(ctx, in)
	if err != nil {
		return nil, fmt.Errorf("failed Slices: %w", err)
	}

	return res, nil
}

func (r *regService) regBasic(ctx context.Context, params json.RawMessage) (interface{}, error) {
	var in service.AliasInt
	if params != nil {
		if err := pjson.Unmarshal(params, &in); err != nil {
			return nil, pjrpc.JRPCErrParseError("failed to parse params")
		}
	}

	res, err := r.svc.Basic(ctx, in)
	if err != nil {
		return nil, fmt.Errorf("failed Basic: %w", err)
	}

	return res, nil
}

func (r *regService) regMap(ctx context.Context, params json.RawMessage) (interface{}, error) {
	var in service.Dictionary
	if params != nil {
		if err := pjson.Unmarshal(params, &in); err != nil {
			return nil, pjrpc.JRPCErrParseError("failed to parse params")
		}
	}

	res, err := r.svc.Map(ctx, in)
	if err != nil {
		return nil, fmt.Errorf("failed Map: %w", err)
	}

	return res, nil
}

func (r *regService) regFromExternalPackage(ctx context.Context, params json.RawMessage) (interface{}, error) {
	in := new(common.Type)
	if params != nil {
		if err := pjson.Unmarshal(params, in); err != nil {
			return nil, pjrpc.JRPCErrParseError("failed to parse params")
		}
	}

	res, err := r.svc.FromExternalPackage(ctx, in)
	if err != nil {
		return nil, fmt.Errorf("failed FromExternalPackage: %w", err)
	}

	return res, nil
}

func (r *regService) regSecondaryUseType(ctx context.Context, params json.RawMessage) (interface{}, error) {
	in := new(service.ReqType)
	if params != nil {
		if err := pjson.Unmarshal(params, in); err != nil {
			return nil, pjrpc.JRPCErrParseError("failed to parse params")
		}
	}

	res, err := r.svc.SecondaryUseType(ctx, in)
	if err != nil {
		return nil, fmt.Errorf("failed SecondaryUseType: %w", err)
	}

	return res, nil
}
