package testdata

import (
	"time"

	"github.com/google/uuid"
	"github.com/shopspring/decimal"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/testdata/service/common"
)

// ErrorData type for field "data" in the Error.
type ErrorData struct {
	SomeMetaData string
}

// IntEnum enum of ints.
type IntEnum int

const (
	IntEnumFirst  IntEnum = iota + 1 // It's 1.
	IntEnumSecond                    // It's 2.
	IntEnumThird                     // It's 3.
)

// StringEnum enum of strings.
type StringEnum string

const (
	StringEnumFirst  StringEnum = "FIRST"  // It's first.
	StringEnumSecond StringEnum = "SECOND" // It's second.
	StringEnumThird  StringEnum = "THIRD"  // It's third.
)

// In docs.
type In struct {
	Int       int             // Field comment.
	UUID      uuid.UUID       // Google uuid.
	Decimal   decimal.Decimal // Decimal.
	IEnum     IntEnum         // Field enum comment.
	SEnum     StringEnum      // Enum field.
	Interface interface{}     // It is 'any' in 1.18.
	Time      time.Time       // Will be RFC3339 string.
}

// Out docs.
type Out struct {
	// Field doc.
	Int         int                    `json:"int,omitempty" xml:"int"` // Field comment.
	Ins         []*In                  `json:"ins" xml:"ins"`
	GoName      string                 `json:",omitempty" xml:"go_name"`
	Field       string                 `json:"" xml:"field"`
	SwaggerType StringEnum             `json:"swagger_type,omitempty" genpjrpc:"type:string,deprecated:true"`
	Map         map[string]interface{} `json:"map"`

	unexported   int
	SkippedByTag int `json:"-" xml:"skipped_by_tag"`
}

// SliceInt docs.
type SliceInt []int

// SliceStruct docs.
type SliceStruct []*In

// AliasInt docs.
type AliasInt int

// AliasString docs.
type AliasString string

// DictionaryEnum keys of the map.
type DictionaryEnum string

const (
	DictionaryEnumFirst  DictionaryEnum = "first"
	DictionaryEnumSecond DictionaryEnum = "second"
)

// Dictionary just Go map.
type Dictionary map[DictionaryEnum]*In

type ReqType struct {
	ReuseTime *common.TimeFilter
	FirstTime *common.ReusedType
}

type RespType struct {
	SecondTime *common.ReusedType
}
