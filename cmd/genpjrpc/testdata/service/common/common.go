package common

import (
	"time"
)

// Type is a type from common package.
type Type struct {
	Field string // Field comment.
}

type TimeFilter struct {
	From time.Time `json:"from,omitempty"`
	To   time.Time `json:"to,omitempty"`
}

type ReusedType struct {
	Field int64
}

// EmptyType just empty no fields here.
type EmptyType struct {
}
