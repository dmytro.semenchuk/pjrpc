package testdata

import (
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/testdata/service/common"
)

// Service desctiption comments.
// Second line of the comments.
type Service interface {
	// Just magic method to set model of the "data" field in the JSON-RPC Error.
	setErrorData(ErrorData)

	// Method comments.
	//genpjrpc:params method_name=special_method_name swagger_tags=FirstTag,SecondTag
	SpecialMethod(In) Out

	// DeprecatedMethod method with mark deprecated.
	//genpjrpc:params deprecated=true
	DeprecatedMethod(In) Out

	Slices(SliceInt) SliceStruct

	Basic(AliasInt) AliasString

	Map(Dictionary) Out

	FromExternalPackage(common.Type) common.EmptyType

	SecondaryUseType(ReqType) RespType
}
