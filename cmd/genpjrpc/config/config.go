// Package config contains configuration of the generator.
package config

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/so_literate/fconfig"
)

// Log configuration.
type Log struct {
	Debug   bool `default:"false" usage:"prints debug messages of the generator"`
	Quiet   bool `default:"false" usage:"does not print any messages other than errors"`
	Pretty  bool `default:"true" usage:"prints pretty log messages"`
	NoColor bool `default:"false" usage:"prints log messages without color"`
}

// Search is a config of the service searching.
type Search struct {
	Name           string `default:"" usage:"name of the interface to generate rpc service"`
	Path           string `default:"." usage:"directory of the searching"`
	KnownTypesPath string `default:"" usage:"path to file with known types"`
}

// Place of the new file.
type Place struct {
	Stdout          bool   `default:"false" usage:"prints generated files to stdout"`
	PathServer      string `default:"./rpcserver" usage:"path to write generated file with server side code"`
	PathClient      string `default:"./rpcclient" usage:"path to write generated file with client side code"`
	PathSwaggerFile string `default:"" usage:"path to file with swagger spec (/path/to/swagger.json)"`
}

// Content additional content of the generated code.
type Content struct {
	HideVersion        bool   `default:"false" usage:"do not prints app version in the generated files"`
	TagsServer         string `default:"" usage:"prints additional tags to server generated file, example: 'go:build some_tag'"`
	TagsClients        string `default:"" usage:"prints additional tags to client generated file, example: 'go:build some_tag'"`
	WithServiceName    bool   `default:"false" usage:"adds service name as prefix of the method name"`
	SwaggerDataPath    string `default:"" usage:"path to file with swagger additional data"`
	SwaggerInfoVersion string `default:"v0.0.0-unknown" usage:"version of the service in the swagger spec 'info.version'"`
}

// Print is a config of the rules and destination of the generated files.
type Print struct {
	Place   Place
	Content Content
}

// Config full configuration of the app.
type Config struct {
	Log    Log
	Search Search
	Print  Print

	Timeout       time.Duration `default:"1m" usage:"timeout for analysis and code printer"`
	Version       bool          `default:"false" usage:"prints version of the app"`
	ExampleConfig bool          `default:"false" usage:"prints example of the json config"`
	ExampleEnv    bool          `default:"false" usage:"prints example of the env config"`
}

// Load parse config params and returns app config.
func Load() (*Config, error) {
	config := new(Config)

	loader := fconfig.New(config).PrefixEnv("GENPJRPC").FileFlagConfig("config").FileNotFoundAlert()

	if err := loader.Load(); err != nil {
		return nil, fmt.Errorf("fconfig.Load: %w", err)
	}

	if config.ExampleConfig {
		loader.PrintFileJSON() // nolint:errcheck // no error here.
		os.Exit(0)
	}

	if config.ExampleEnv {
		loader.PrintEnvDefaults() // nolint:errcheck // no error here.
		os.Exit(0)
	}

	return config, nil
}
