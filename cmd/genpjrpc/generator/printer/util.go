package printer

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"golang.org/x/tools/imports"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

func printOptionalNewLineInInterfaceMethod(dst *bytes.Buffer, method *model.Method, currentMethod int) {
	if currentMethod == 0 {
		return
	}

	if method.Comments == "" {
		return
	}

	dst.WriteByte('\n')
}

// ExtractImportPathFromPath returns right part of the abs path from first part of the import path.
// Also returns the last element of the abs path as a package name.
func ExtractImportPathFromPath(absPath, importPath string) (newImportPath, packageName string) {
	absParts := strings.Split(absPath, string(os.PathSeparator))
	packageName = absParts[len(absParts)-1]
	importParts := strings.Split(importPath, "/")
	importStart := importParts[0]

	for i, abs := range absParts {
		if abs == importStart {
			return strings.Join(absParts[i:], "/"), packageName
		}
	}

	// can't find prefix match, so try to find something common.
	for absIndex, abs := range absParts {
		for impIndex, imp := range importParts {
			if abs == imp {
				path := importParts[:impIndex]
				path = append(path, absParts[absIndex:]...)
				return strings.Join(path, "/"), packageName
			}
		}
	}

	return "", ""
}

func (p *Printer) setCurrentImportPath(service *model.Service, dst string) (packageName string, err error) {
	dst, err = filepath.Abs(dst)
	if err != nil {
		return "", fmt.Errorf("filepath.Abs: %w", err)
	}

	var importPath string

	if p.conf.Place.Stdout {
		importPath = ""
		packageName = filepath.Base(dst)
	} else {
		importPath, packageName = ExtractImportPathFromPath(dst, service.Import)
		if importPath == "" {
			return "", fmt.Errorf(
				"%w: can't to get import path of the destination path (abs %q, import %q)",
				ErrInvalidData, dst, service.Import,
			)
		}

		if importPath == service.Import {
			packageName = service.PkgName
		}
	}

	p.importer.SetCurrentImportPath(importPath)

	p.log.Debug().Str("importPath", importPath).Str("packageName", packageName).Msg("setCurrentImportPath")

	return packageName, nil
}

func unpackArgumentType(arg *model.Argument) (res *model.Argument, isPointerType bool) {
	if arg.NamedObject.IsStruct() {
		isPointerType = true
	}

	return arg, isPointerType
}

func printComments(dst *bytes.Buffer, comment string, tabsCount int) {
	scanner := bufio.NewScanner(strings.NewReader(comment))

	tabPrefix := ""
	for i := 0; i < tabsCount; i++ {
		tabPrefix += "\t"
	}

	for scanner.Scan() {
		fmt.Fprintf(dst, "%s// %s\n", tabPrefix, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		panic("scanner.Err: " + err.Error())
	}
}

func printDeprecatedComments(dst *bytes.Buffer, tabsCount int) {
	printComments(dst, "\nDeprecated: Do not use.\n", tabsCount)
}

func getLocalPrefix(importPath string) string {
	parts := strings.Split(importPath, "/")
	if len(parts) <= 3 {
		return importPath
	}

	return strings.Join(parts[:3], "/")
}

func formatGoCode(buf *bytes.Buffer, localPrefix string) error {
	imports.LocalPrefix = localPrefix

	res, err := imports.Process("", buf.Bytes(), nil)
	if err != nil {
		return fmt.Errorf("imports.Process: %w", err)
	}

	buf.Reset()

	if _, err = buf.Write(res); err != nil {
		return fmt.Errorf("buf.Write: %w", err)
	}

	return nil
}

// SetStringIfEmpty sets string value into pointer of string if target value is empty.
func SetStringIfEmpty(target *string, value string) {
	if *target == "" {
		*target = value
	}
}

// GetFirstLine returns first line of the text.
func GetFirstLine(text string) string {
	scanner := bufio.NewScanner(strings.NewReader(text))

	for scanner.Scan() {
		return scanner.Text()
	}

	return ""
}
