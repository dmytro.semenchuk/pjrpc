package printer

import (
	"fmt"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

func isMethodArgumentValid(arg *model.Argument) error {
	if arg.IsNamed() {
		return nil
	}

	return fmt.Errorf("%w: argument should be named type or pointer to named type: %#v", ErrInvalidData, arg)
}

func isMethodValid(method *model.Method) error {
	if method.Parameter == nil {
		return fmt.Errorf("%w: parameter is empty", ErrInvalidData)
	}

	if err := isMethodArgumentValid(method.Parameter); err != nil {
		return fmt.Errorf("parameter is invalid: %w", err)
	}

	if method.Result == nil {
		return fmt.Errorf("%w: result is empty", ErrInvalidData)
	}

	if err := isMethodArgumentValid(method.Result); err != nil {
		return fmt.Errorf("result is invalid: %w", err)
	}

	return nil
}

func isServiceValid(service *model.Service) error {
	if service.ErrorData != nil {
		if err := isMethodArgumentValid(service.ErrorData); err != nil {
			return fmt.Errorf("error data is invalid: %w", err)
		}
	}

	for i, method := range service.Methods {
		if err := isMethodValid(method); err != nil {
			return fmt.Errorf("method [%d %s] is invalid: %w", i+1, method.GoName, err)
		}
	}

	return nil
}
