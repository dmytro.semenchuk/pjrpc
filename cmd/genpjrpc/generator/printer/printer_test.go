package printer_test

import (
	"bytes"
	"encoding/json"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
	"gitlab.com/so_literate/gentools/logger"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/config"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/printer"
)

func getLogger() (*bytes.Buffer, *zerolog.Logger) {
	buf := bytes.NewBuffer([]byte("\n"))

	conf := &logger.Config{
		Debug:   true,
		Pretty:  true,
		NoColor: true,
		Writer:  buf,
	}

	log := logger.New(conf)

	return buf, log
}

var (
	importServicePath      = "gitlab.com/repo/project/pkg/service"
	importServiceModelPath = "gitlab.com/repo/project/pkg/service/model"

	service = &model.Service{
		Import:   importServicePath,
		PkgName:  "model",
		Name:     "service",
		GoName:   "Service",
		Comments: "Service comments.\nSecond line.\n",
		ErrorData: &model.Argument{
			NamedImport:  importServiceModelPath,
			NamedName:    "ErrorData",
			NamedComment: "Data field of the JSON-RPC error.\n",
			NamedObject: &model.Argument{
				StructFields: []*model.Argument{
					{
						StructFieldName:    "code",
						StructFieldGoName:  "Code",
						StructFieldComment: "Additional error code.\n",
						BasicKind:          model.ArgumentKindInt64,
					},
				},
			},
		},
		Methods: []*model.Method{
			{
				Name:         "some_work",
				GoName:       "SomeWork",
				Comments:     "SomeWork comments of the method.\nSecond line.\n",
				IsDeprecated: true,
				Parameter: &model.Argument{
					NamedImport:  importServiceModelPath,
					NamedName:    "In",
					NamedComment: "In is an input param\nof the service's method\n",
					NamedObject: &model.Argument{
						StructFields: []*model.Argument{
							{
								StructFieldName:    "int",
								StructFieldGoName:  "Int",
								StructFieldComment: "Field comment.\n",
								BasicKind:          model.ArgumentKindInt,
							},
						},
					},
				},
				Result: &model.Argument{
					NamedImport:  importServiceModelPath,
					NamedName:    "Out",
					NamedComment: "Out is an output param\nof the service's method\n",
					NamedObject: &model.Argument{
						StructFields: []*model.Argument{
							{
								StructFieldName:       "enum",
								StructFieldGoName:     "Enum",
								StructFieldComment:    "",
								StructFieldIsOptional: false,
								NamedImport:           importServiceModelPath,
								NamedName:             "IntEnum",
								NamedComment:          "IntEnum enum of ints.\n",
								NamedObject: &model.Argument{
									EnumBasicKind: model.ArgumentKindInt,
									EnumElements: []*model.Argument{
										{
											EnumElementName:    "IntEnumFirst",
											EnumElementValue:   "1",
											EnumElementComment: "It's 1.\n",
										},
										{
											EnumElementName:    "IntEnumSecond",
											EnumElementValue:   "2",
											EnumElementComment: "It's 2.\n",
										},
										{
											EnumElementName:    "IntEnumThird",
											EnumElementValue:   "3",
											EnumElementComment: "It's 3.\n",
										},
									},
								},
							},
							{
								StructFieldName:       "slice",
								StructFieldGoName:     "Slice",
								StructFieldComment:    "",
								StructFieldIsOptional: true,
								SliceElement: &model.Argument{
									BasicKind: model.ArgumentKindInt,
								},
							},
							{
								StructFieldName:       "time",
								StructFieldGoName:     "Time",
								StructFieldComment:    "It is a RFC3339 string",
								StructFieldIsOptional: true,
								BasicKind:             model.ArgumentKindString,
								BasicFormat:           model.ArgumentFormatTime,
							},
							{
								StructFieldName:       "uuid",
								StructFieldGoName:     "UUID",
								StructFieldComment:    "It is a UUID string",
								StructFieldIsOptional: true,
								BasicKind:             model.ArgumentKindString,
								BasicFormat:           model.ArgumentFormatUUID,
							},
						},
					},
				},
			},
			{
				Name:         "not_pointer_types",
				GoName:       "NotPointerTypes",
				Comments:     "",
				IsDeprecated: false,
				Parameter: &model.Argument{
					NamedImport:  importServiceModelPath,
					NamedName:    "BasicType",
					NamedComment: "",
					NamedObject: &model.Argument{
						BasicKind: model.ArgumentKindInt64,
					},
				},
				Result: &model.Argument{
					NamedImport:  importServiceModelPath,
					NamedName:    "SliceType",
					NamedComment: "",
					NamedObject: &model.Argument{
						SliceElement: &model.Argument{
							BasicKind: model.ArgumentKindString,
						},
					},
				},
			},
			{
				Name:         "dictionary_map",
				GoName:       "DictionaryMap",
				Comments:     "",
				IsDeprecated: false,
				Parameter: &model.Argument{
					NamedImport:  importServiceModelPath,
					NamedName:    "Dictionary",
					NamedComment: "",
					NamedObject: &model.Argument{
						MapKey: &model.Argument{
							NamedImport:  importServiceModelPath,
							NamedName:    "MapEnum",
							NamedComment: "MapEnum key of the map.\n",
							NamedObject: &model.Argument{
								EnumBasicKind: model.ArgumentKindString,
								EnumElements: []*model.Argument{
									{EnumElementName: "MapKey1", EnumElementValue: "map_key_1", EnumElementComment: "mk1"},
									{EnumElementName: "MapKey2", EnumElementValue: "map_key_2", EnumElementComment: "mk2"},
								},
							},
						},
						MapElem: &model.Argument{
							NamedImport: importServiceModelPath,
							NamedName:   "StructForMap",
							NamedObject: &model.Argument{
								StructFields: []*model.Argument{
									{
										StructFieldName:   "map_field",
										StructFieldGoName: "MapField",
										BasicKind:         model.ArgumentKindInt,
									},
								},
							},
						},
					},
				},
				Result: &model.Argument{
					NamedImport: importServiceModelPath,
					NamedName:   "StructWithDictionary",
					NamedObject: &model.Argument{
						StructFields: []*model.Argument{
							{
								StructFieldName:   "map_field",
								StructFieldGoName: "MapField",
								MapKey: &model.Argument{
									BasicKind: model.ArgumentKindString,
								},
								MapElem: &model.Argument{
									IsEmptyInterface: true,
								},
							},
						},
					},
				},
			},
		},
	}
)

func TestRun(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	conf := &config.Print{
		Place: config.Place{Stdout: true},
		Content: config.Content{
			SwaggerInfoVersion: "v0.0.0-unknown", // default value.
			WithServiceName:    true,
			SwaggerDataPath:    filepath.Join("testdata", "swagger_data.json"),
		},
	}

	testCase := func(wantFile string) {
		stdout := new(bytes.Buffer)

		buf, log := getLogger()

		p := printer.New(log, conf, stdout, "v0.0.1-go_test")

		err := p.Run(service)
		so.NoError(err)

		t.Logf("log of test %s:%s\n", wantFile, buf.String())

		want, err := os.ReadFile(wantFile)
		so.NoError(err)

		if strings.HasSuffix(wantFile, ".json") {
			// I want to store test file as pretty json.
			result := stdout.String() // detach buffer sllice.
			stdout.Reset()
			so.NoError(json.Indent(stdout, []byte(result), "", "  "))
		}

		// some times it required: t.Logf("gen file %s:\n%s\n", wantFile, stdout.String())
		so.Equal(string(want), stdout.String())
	}

	conf.Place.PathServer = filepath.Join("out", "server")
	testCase(filepath.Join("testdata", "server_file.go"))

	conf.Place.PathServer = ""
	conf.Place.PathClient = filepath.Join("out", "client")
	testCase(filepath.Join("testdata", "client_file.go"))

	conf.Place.PathClient = ""
	conf.Place.PathSwaggerFile = filepath.Join("out", "swagger", "swagger.json")
	testCase(filepath.Join("testdata", "swagger.json"))
}
