package printer

import (
	"bytes"
	"fmt"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/printer/tmpl"
)

func (p *Printer) generateServerMethodConstants(dst *bytes.Buffer, serivce *model.Service) {
	dst.WriteString("// List of the server JSON-RPC methods.\n")
	dst.WriteString("const (\n")

	constNameData := &tmpl.MethodConstantNameData{ConstSuffix: ""}
	constValueData := &tmpl.MethodConstantValueData{}

	if p.conf.Content.WithServiceName {
		constNameData.GoServiceName = serivce.GoName
		constValueData.JRPCServiceName = serivce.Name
	}

	p.serverMethodConstants = make(map[*model.Method]*serverMethodNames, len(serivce.Methods))

	for _, method := range serivce.Methods {
		constNameData.GoMethodName = method.GoName
		constValueData.JRPCMethodName = method.Name

		constName := tmpl.ExecMethodConstantName(constNameData)
		constValue := tmpl.ExecMethodConstantValue(constValueData)

		p.serverMethodConstants[method] = &serverMethodNames{
			constantName: constName,
			regFuncName:  fmt.Sprintf("reg%s", method.GoName),
		}

		fmt.Fprintf(dst, "\t%s = %q\n", constName, constValue)
	}

	dst.WriteString(")\n\n")
}

func (p *Printer) generateInterfaceMethod(dst *bytes.Buffer, method *model.Method, modType string, tabCount int) {
	in, inIsPointer := unpackArgumentType(method.Parameter)
	out, outIsPointer := unpackArgumentType(method.Result)

	data := &tmpl.InterfaceMethodData{
		MethodName:   method.GoName,
		ContextType:  p.importer.GetQualifiedObject(pkgContext, "Context"),
		InIsPointer:  inIsPointer,
		InType:       p.importer.GetQualifiedObject(in.NamedImport, in.NamedName),
		OutType:      p.importer.GetQualifiedObject(out.NamedImport, out.NamedName),
		OutIsPointer: outIsPointer,
		ModType:      modType,
	}

	if method.Comments != "" {
		printComments(dst, method.Comments, tabCount)
	}

	if method.IsDeprecated {
		printDeprecatedComments(dst, tabCount)
	}

	fmt.Fprintf(dst, "\t%s\n", tmpl.ExecInterfaceMethod(data))
}

func (p *Printer) generateServerInterface(dst *bytes.Buffer, serivce *model.Service) string {
	interfaceName := fmt.Sprintf("%sServer", serivce.GoName)

	fmt.Fprintf(dst, "// %s is an API server for %s service.\ntype %s interface {\n", interfaceName, serivce.GoName, interfaceName)

	for i, method := range serivce.Methods {
		printOptionalNewLineInInterfaceMethod(dst, method, i)
		p.generateInterfaceMethod(dst, method, "", 1)
	}

	dst.WriteString("}\n\n")

	return interfaceName
}

func (p *Printer) generateServerRegister(dst *bytes.Buffer, interfaceName string, serivce *model.Service) {
	// Prints register type.
	regTypeData := &tmpl.ServerRegTypeData{
		RegTypeName:   fmt.Sprintf("reg%s", serivce.GoName),
		InterfaceName: interfaceName,
	}

	tmpl.ExecServerRegType(dst, regTypeData)

	// Prints register function.
	regFuncData := &tmpl.ServerRegFuncData{
		FuncName:       fmt.Sprintf("Register%s", regTypeData.InterfaceName),
		SType:          p.importer.GetQualifiedObject(pkgPJRPC, "Registrator"),
		InterfaceName:  regTypeData.InterfaceName,
		MiddlewareType: p.importer.GetQualifiedObject(pkgPJRPC, "Middleware"),
		RegTypeName:    regTypeData.RegTypeName,
		Methods:        make([]*tmpl.ServerRegFuncDataMethod, len(serivce.Methods)),
	}

	for i, method := range serivce.Methods {
		names := p.serverMethodConstants[method]

		regFuncData.Methods[i] = &tmpl.ServerRegFuncDataMethod{
			ConstantName: names.constantName,
			RegFuncName:  names.regFuncName,
		}
	}

	tmpl.ExecServerRegFuncTmpl(dst, regFuncData)

	// Prints register methods.
	regMethodData := &tmpl.ServerRegMethodData{
		RegTypeName: regTypeData.RegTypeName,
		RegFuncName: "",
		ContextType: p.importer.GetQualifiedObject(pkgContext, "Context"), // context.Context.
		JSONParams:  p.importer.GetQualifiedObject(pkgJSON, "RawMessage"), // json.RawMessage.
		InType:      "",
		PJSONMethod: p.importer.GetQualifiedObject(pkgPJRPCPjson, "Unmarshal"),    // pjson.Unmarshal.
		JerrsMethod: p.importer.GetQualifiedObject(pkgPJRPC, "JRPCErrParseError"), // pjrpc.JRPCErrParseError.
		MethodName:  "",
		FmtErrorf:   p.importer.GetQualifiedObject(pkgFmt, "Errorf"),
	}

	for _, method := range serivce.Methods {
		names := p.serverMethodConstants[method]
		in, inIsPointer := unpackArgumentType(method.Parameter)

		regMethodData.RegFuncName = names.regFuncName
		regMethodData.InIsPointer = inIsPointer
		regMethodData.InType = p.importer.GetQualifiedObject(in.NamedImport, in.NamedName)
		regMethodData.MethodName = method.GoName

		tmpl.ExecServerRegMethodTmpl(dst, regMethodData)
	}
}

// nolint:dupl // it looks like generateClient but it is ok.
func (p *Printer) generateServer(serivce *model.Service) error {
	if p.conf.Place.PathServer == "" || p.conf.Place.PathServer == emptyString {
		return nil
	}

	p.serverFile = new(bytes.Buffer)

	packageName, err := p.setCurrentImportPath(serivce, p.conf.Place.PathServer)
	if err != nil {
		return fmt.Errorf("setCurrentImportPath: %w", err)
	}

	p.printHeaderGoFile(p.serverFile, packageName, p.conf.Content.TagsServer)

	fileBody := new(bytes.Buffer)

	// Constants.
	p.generateServerMethodConstants(fileBody, serivce)

	// Server interface.
	interfaceName := p.generateServerInterface(fileBody, serivce)

	// Register methods.
	p.generateServerRegister(fileBody, interfaceName, serivce)

	// Write import block and whole file's content into file.
	p.serverFile.WriteString(p.importer.GenerateFileImport() + "\n")

	fileBody.WriteTo(p.serverFile) // nolint:errcheck,gosec // it is just two bytes.Buffer objects.

	if err = formatGoCode(p.serverFile, getLocalPrefix(serivce.Import)); err != nil {
		return fmt.Errorf("formatGoCode: %w\n\n%s", err, p.serverFile.String())
	}

	return nil
}
