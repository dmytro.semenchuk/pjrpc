// Package tmpl contains templates of the printer.
package tmpl

import (
	"bytes"
	"text/template"
)

func mustExec(err error) {
	if err != nil {
		panic("failed to exec template: " + err.Error())
	}
}

// HeaderGoFileData data of the go header template.
type HeaderGoFileData struct {
	Version string
	Package string
	Tags    string
}

var headerGoFileTmpl = template.Must(template.New("tmpl").
	Parse("// Code generated by genpjrpc. DO NOT EDIT.\n" +
		"{{if .Version}}//  genpjrpc version: {{.Version}}\n{{end}}" +
		"{{if .Tags}}//{{.Tags}}\n{{end}}" +
		"\npackage {{.Package}}\n\n",
	))

// ExecHeaderGoFile executes headerGoFileTmpl into buffer with data.
func ExecHeaderGoFile(dst *bytes.Buffer, data *HeaderGoFileData) {
	mustExec(headerGoFileTmpl.Execute(dst, data))
}

// MethodConstantNameData data of the methodConstantName template.
type MethodConstantNameData struct {
	GoServiceName string
	GoMethodName  string
	ConstSuffix   string
}

var methodConstantNameTmpl = template.Must(template.New("tmpl").
	Parse("JSONRPCMethod{{.GoServiceName}}{{.GoMethodName}}{{.ConstSuffix}}"))

// ExecMethodConstantName executes methodConstantTmpl into buffer with data.
func ExecMethodConstantName(data *MethodConstantNameData) string {
	buf := bytes.Buffer{}
	mustExec(methodConstantNameTmpl.Execute(&buf, data))
	return buf.String()
}

// MethodConstantValueData data of the methodConstantValueTmpl template.
type MethodConstantValueData struct {
	JRPCServiceName string
	JRPCMethodName  string
}

var methodConstantValueTmpl = template.Must(template.New("tmpl").
	Parse("{{if .JRPCServiceName}}{{.JRPCServiceName}}_{{end}}{{.JRPCMethodName}}"))

// ExecMethodConstantValue executes methodConstantTmpl into buffer with data.
func ExecMethodConstantValue(data *MethodConstantValueData) string {
	buf := bytes.Buffer{}
	mustExec(methodConstantValueTmpl.Execute(&buf, data))
	return buf.String()
}

// InterfaceMethodData data of the interfaceMethodTmpl template.
type InterfaceMethodData struct {
	MethodName   string
	ContextType  string
	InIsPointer  bool
	InType       string
	ResultName   string
	OutIsPointer bool
	OutType      string
	ErrName      string
	ModType      string
}

var interfaceMethodTmpl = template.Must(template.New("tmpl").
	Parse("{{.MethodName}}" +
		"(ctx {{.ContextType}}, in {{if .InIsPointer}}*{{end}}{{.InType}}{{if .ModType}}, mods ...{{.ModType}}{{end}})" +
		" ({{.ResultName}} {{if .OutIsPointer}}*{{end}}{{.OutType}}, {{.ErrName}} error)",
	))

// ExecInterfaceMethod executes interfaceMethodTmpl into buffer with data.
func ExecInterfaceMethod(data *InterfaceMethodData) string {
	buf := bytes.Buffer{}
	mustExec(interfaceMethodTmpl.Execute(&buf, data))
	return buf.String()
}

// ServerRegTypeData data of the serverRegTypeTmpl template.
type ServerRegTypeData struct {
	RegTypeName   string
	InterfaceName string
}

var serverRegTypeTmpl = template.Must(template.New("tmpl").
	Parse("type {{.RegTypeName}} struct {\n" +
		"\tsvc {{.InterfaceName}}\n" +
		"}\n\n",
	))

// ExecServerRegType executes serverRegTypeTmpl into buffer with data.
func ExecServerRegType(dst *bytes.Buffer, data *ServerRegTypeData) {
	mustExec(serverRegTypeTmpl.Execute(dst, data))
}

// ServerRegFuncDataMethod part of the ServerRegFuncData.
type ServerRegFuncDataMethod struct {
	ConstantName string
	RegFuncName  string
}

// ServerRegFuncData data of the serverRegFuncTmpl template.
type ServerRegFuncData struct {
	FuncName       string
	SType          string
	InterfaceName  string
	MiddlewareType string
	RegTypeName    string
	Methods        []*ServerRegFuncDataMethod
}

var serverRegFuncTmpl = template.Must(template.New("tmpl").
	Parse("// {{.FuncName}} registers rpc handlers with middlewares in the server router.\n" +
		"func {{.FuncName}}(srv {{.SType}}, svc {{.InterfaceName}}, middlewares ...{{.MiddlewareType}}) {\n" +
		"\tr := &{{.RegTypeName}}{svc: svc}\n\n" +
		"{{range .Methods}}" +
		"\tsrv.RegisterMethod({{.ConstantName}}, r.{{.RegFuncName}})\n" +
		"{{end}}\n" +
		"\tsrv.With(middlewares...)\n" +
		"}\n\n",
	))

// ExecServerRegFuncTmpl executes serverRegFuncTmpl into buffer with data.
func ExecServerRegFuncTmpl(dst *bytes.Buffer, data *ServerRegFuncData) {
	mustExec(serverRegFuncTmpl.Execute(dst, data))
}

// ServerRegMethodData data of the serverRegMethodTmpl template.
type ServerRegMethodData struct {
	RegTypeName string
	RegFuncName string
	ContextType string
	JSONParams  string
	InIsPointer bool
	InType      string
	PJSONMethod string
	JerrsMethod string
	MethodName  string
	FmtErrorf   string
}

var serverRegMethodTmpl = template.Must(template.New("tmpl").
	Parse("func (r *{{.RegTypeName}}) {{.RegFuncName}}(ctx {{.ContextType}}, params {{.JSONParams}}) (interface{}, error) {\n" +
		"\t{{if .InIsPointer}}in := new({{.InType}}){{else}}var in {{.InType}}{{end}}\n" +
		"\tif params != nil {\n" +
		"\t\tif err := {{.PJSONMethod}}(params, {{if not .InIsPointer}}&{{end}}in); err != nil {\n" +
		"\t\t\treturn nil, {{.JerrsMethod}}(\"failed to parse params\")\n" +
		"\t\t}\n" +
		"\t}\n\n" +
		"\tres, err := r.svc.{{.MethodName}}(ctx, in)\n" +
		"\tif err != nil {\n" +
		"\t\treturn nil, {{.FmtErrorf}}(\"failed {{.MethodName}}: %w\", err)\n" +
		"\t}\n" +
		"\n" +
		"\treturn res, nil\n" +
		"}\n\n",
	))

// ExecServerRegMethodTmpl executes serverRegMethodTmpl into buffer with data.
func ExecServerRegMethodTmpl(dst *bytes.Buffer, data *ServerRegMethodData) {
	mustExec(serverRegMethodTmpl.Execute(dst, data))
}

// ClientImplementorData data of the clientImplementorTmpl template.
type ClientImplementorData struct {
	Implementor   string
	InvokerType   string
	InterfaceName string
	ServiceName   string
}

var clientImplementorTmpl = template.Must(template.New("tmpl").
	Parse("type {{.Implementor}} struct {\n" +
		"\tcl {{.InvokerType}}\n" +
		"}\n\n" +
		"// New{{.InterfaceName}} returns new client implementation of the {{.ServiceName}} service.\n" +
		"func New{{.InterfaceName}}(cl {{.InvokerType}}) {{.InterfaceName}} {\n" +
		"\treturn &{{.Implementor}}{cl: cl}\n" +
		"}\n\n",
	))

// ExecClientImplementorTmpl executes clientImplementorTmpl into buffer with data.
func ExecClientImplementorTmpl(dst *bytes.Buffer, data *ClientImplementorData) {
	mustExec(clientImplementorTmpl.Execute(dst, data))
}

// ClientImplementorMethodData data of the clientImplementorTmpl template.
type ClientImplementorMethodData struct {
	Implementor  string
	MethodFunc   string
	ResultVar    string
	UUIDFunc     string
	FmtErrorf    string
	OutIsPointer bool
	OutType      string
	ConstantName string
}

var clientImplementorMethodTmpl = template.Must(template.New("tmpl").
	Parse("func (c *{{.Implementor}}) {{.MethodFunc}} {\n" +
		"\tgen, err := {{.UUIDFunc}}()\n" +
		"\tif err != nil {\n" +
		"\t\treturn {{.ResultVar}}, {{.FmtErrorf}}(\"failed to create uuid generator: %w\", err)\n" +
		"\t}\n" +
		"\n" +
		"\terr = c.cl.Invoke(ctx, gen.String(), {{.ConstantName}}, in, &{{.ResultVar}}, mods...)\n" +
		"\tif err != nil {\n" +
		"\t\treturn {{.ResultVar}}, {{.FmtErrorf}}(\"failed to Invoke method %q: %w\", {{.ConstantName}}, err)\n" +
		"\t}\n" +
		"\n" +
		"\treturn {{.ResultVar}}, nil\n" +
		"}\n\n",
	))

// ExecClientImplementorMethodTmpl executes clientImplementorMethodTmpl into buffer with data.
func ExecClientImplementorMethodTmpl(dst *bytes.Buffer, data *ClientImplementorMethodData) {
	mustExec(clientImplementorMethodTmpl.Execute(dst, data))
}
