// Package printer prints generated code into go files.
package printer

import (
	"bytes"
	"fmt"
	"io"
	"path/filepath"

	"github.com/rs/zerolog"

	"gitlab.com/so_literate/gentools/importer"
	"gitlab.com/so_literate/gentools/writer"
	"gitlab.com/so_literate/openapi"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/config"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/printer/tmpl"
)

const (
	pkgFmt         = "fmt"
	pkgContext     = "context"
	pkgJSON        = "encoding/json"
	pkgPJRPC       = "gitlab.com/pjrpc/pjrpc/v2"
	pkgPJRPCPjson  = "gitlab.com/pjrpc/pjrpc/v2/pjson"
	pkgPJRPCClient = "gitlab.com/pjrpc/pjrpc/v2/client"
	pkgUUID        = "github.com/google/uuid"
)

var pkgsToReg = map[string]string{
	pkgFmt:         "",
	pkgContext:     "",
	pkgJSON:        "",
	pkgPJRPC:       "pjrpc",
	pkgPJRPCPjson:  "",
	pkgPJRPCClient: "",
	pkgUUID:        "",
}

// emptyString it is a way to pass as flag value: -flag="".
const emptyString = `""`

type serverMethodNames struct {
	constantName string
	regFuncName  string
}

// Printer contains method to print code.
type Printer struct {
	log      zerolog.Logger
	conf     *config.Print
	importer *importer.Importer
	stdout   io.Writer
	version  string

	serverFile  *bytes.Buffer
	clientFile  *bytes.Buffer
	swaggerFile *bytes.Buffer

	serverMethodConstants map[*model.Method]*serverMethodNames
	clientMethodConstants map[*model.Method]string
	swaggerNamedSchemas   map[string]*openapi.Schema
}

// New creates new printer object.
func New(log *zerolog.Logger, conf *config.Print, stdout io.Writer, version string) *Printer {
	p := &Printer{
		log:      log.With().Bool("printer", true).Logger(),
		conf:     conf,
		importer: importer.New(),
		stdout:   stdout,

		version: version,
	}

	return p
}

// Run generetes code and prints it to go files.
func (p *Printer) Run(service *model.Service) error {
	for pkg, alias := range pkgsToReg {
		p.importer.RegisterImport(alias, pkg)
	}

	if err := isServiceValid(service); err != nil {
		return fmt.Errorf("service is invalid: %w", err)
	}

	if err := p.generateServer(service); err != nil {
		return fmt.Errorf("failed to generate server side: %w", err)
	}

	if err := p.generateClient(service); err != nil {
		return fmt.Errorf("failed to generate client side: %w", err)
	}

	if err := p.generateSwagger(service); err != nil {
		return fmt.Errorf("failed to generate swagger spec: %w", err)
	}

	if err := p.writeFiles(service); err != nil {
		return fmt.Errorf("failed to write files: %w", err)
	}

	return nil
}

func (p *Printer) writeFile(content *bytes.Buffer, dst string) error {
	var out io.WriteCloser
	var err error

	if p.conf.Place.Stdout {
		out = writer.GetWriter(p.stdout)
	} else {
		out, err = writer.GetFileWriter(dst)
	}

	if err != nil {
		return fmt.Errorf("GetFileWriter: %w", err)
	}

	defer out.Close() // nolint:errcheck,gosec // I'm don't care about this error.

	_, err = content.WriteTo(out)
	if err != nil {
		return fmt.Errorf("failed to write content (stdout: %v, dst: %q): %w", p.conf.Place.Stdout, dst, err)
	}

	absDst, err := filepath.Abs(dst)
	if err != nil {
		absDst = dst
	}

	p.log.Info().Msgf("Generated file: %s", absDst)

	return nil
}

func (p *Printer) writeFiles(service *model.Service) error {
	if p.serverFile != nil {
		err := p.writeFile(
			p.serverFile,
			filepath.Join(p.conf.Place.PathServer, fmt.Sprintf("pjrpc_server_%s.go", service.Name)),
		)
		if err != nil {
			return fmt.Errorf("writeFile server: %w", err)
		}
	}

	if p.clientFile != nil {
		err := p.writeFile(
			p.clientFile,
			filepath.Join(p.conf.Place.PathClient, fmt.Sprintf("pjrpc_client_%s.go", service.Name)),
		)
		if err != nil {
			return fmt.Errorf("writeFile server: %w", err)
		}
	}

	if p.swaggerFile != nil {
		err := p.writeFile(
			p.swaggerFile,
			p.conf.Place.PathSwaggerFile,
		)
		if err != nil {
			return fmt.Errorf("writeFile server: %w", err)
		}
	}

	return nil
}

func (p *Printer) printHeaderGoFile(dst *bytes.Buffer, packageName, tags string) {
	data := &tmpl.HeaderGoFileData{
		Package: packageName,
		Tags:    tags,
	}

	if !p.conf.Content.HideVersion {
		data.Version = p.version
	}

	tmpl.ExecHeaderGoFile(dst, data)
}
