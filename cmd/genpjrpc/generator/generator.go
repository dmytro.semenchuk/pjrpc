// Package generator contains analyzer of the source code and new code printer.
package generator

import (
	"context"
	"fmt"
	"io"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/so_literate/gentools/logger"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/config"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/analyzer"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/printer"
)

const (
	version = "v0.2.0"
	appName = "genpjrpc"
)

// Generator main type runs analyzer and code printer.
type Generator struct {
	Conf   *config.Config
	Log    *zerolog.Logger
	Stdout io.Writer
}

// New returns generator with logger ready to run.
func New(conf *config.Config, stdout io.Writer) (g *Generator, err error) {
	loggerConfig := logger.Config{
		Debug:   conf.Log.Debug,
		Quiet:   conf.Log.Quiet,
		Pretty:  conf.Log.Pretty,
		NoColor: conf.Log.NoColor,
	}

	g = &Generator{
		Conf:   conf,
		Log:    logger.New(&loggerConfig),
		Stdout: stdout,
	}

	return g, nil
}

// Run runs analyzer and code printer.
func (g *Generator) Run(ctx context.Context) error {
	if g.Conf.Version {
		g.Log.Info().Msgf("%s: %s", appName, version)
		return nil
	}

	g.Log.Info().Msgf("Starting %s: %s", appName, version)

	if g.Conf.Timeout == 0 {
		g.Conf.Timeout = time.Minute
	}

	a, err := analyzer.New(g.Log, &g.Conf.Search)
	if err != nil {
		return fmt.Errorf("analyzer.New: %w", err)
	}

	p := printer.New(g.Log, &g.Conf.Print, g.Stdout, version)

	var cancel context.CancelFunc
	ctx, cancel = context.WithTimeout(ctx, g.Conf.Timeout)
	defer cancel()

	done := make(chan error)

	go func() {
		service, err := a.Run(ctx)
		if err != nil {
			done <- fmt.Errorf("analyzer: %w", err)
			return
		}

		err = p.Run(service)
		if err != nil {
			done <- fmt.Errorf("printer: %w", err)
			return
		}

		done <- nil
	}()

	select {
	case <-ctx.Done():
		return fmt.Errorf("context done: %w", ctx.Err())

	case err := <-done:
		if err != nil {
			return fmt.Errorf("generator error: %w", err)
		}
	}

	return nil
}
