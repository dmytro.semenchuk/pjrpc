// Package model contains models to pass data between analyzer and printer.
package model

// Argument of the method Parameter or Result.
type Argument struct {
	PointerElement *Argument // Type has a star (pointer) to the type.

	NamedImport  string    // Path of the package with declarated named type.
	NamedName    string    // Name of the named type.
	NamedComment string    // Comments of the NamedObject.
	NamedObject  *Argument // Unwrapped argument of the named type.

	EnumBasicKind      ArgumentKind // Basic type of the Enum (named type).
	EnumElements       []*Argument  // List of values with comments of the enum.
	EnumElementName    string       // Name of the enum element.
	EnumElementValue   string       // Value of the enum element.
	EnumElementComment string       // Comment of the enum element.

	StructFields            []*Argument // List of the struct's fields.
	StructFieldName         string      // Name of the struct's field in json.
	StructFieldGoName       string      // Origin name of the struct's field.
	StructFieldComment      string      // Comment of the struct's field.
	StructFieldIsOptional   bool        // Filed of the struct has omitempty json tag.
	StructFieldIsDeprecated bool        // Filed of the struct has deprecated tag mark.

	SliceElement *Argument // Element of the slice.

	MapKey  *Argument // Key of the map.
	MapElem *Argument // Element of the map.

	BasicKind   ArgumentKind   // Basic type of the argument.
	BasicFormat ArgumentFormat // Additional format of the argument.

	IsEmptyInterface bool // Argument is an empty interface or any (1.18).
}

// IsPointer returns true when the argument is a Pointer to type.
//   func(a *SomeType)
func (a *Argument) IsPointer() bool {
	return a.PointerElement != nil
}

// IsNamed returns true when the argument is a Named Type.
//   type NamedType struct {
func (a *Argument) IsNamed() bool {
	return a.NamedObject != nil
}

// IsEnum returns true when the argument is a Named Object with Enum values.
func (a *Argument) IsEnum() bool {
	return a.EnumBasicKind != 0
}

// IsStruct returns true when the argument is a Struct.
//   struct{...}
func (a *Argument) IsStruct() bool {
	return a.StructFields != nil
}

// IsSlice returns true when the argument is a Slice.
//   []*Named or []int
func (a *Argument) IsSlice() bool {
	return a.SliceElement != nil
}

// IsBasic returns true when the argument is a Basic type.
//   int, string, float64, ...
func (a *Argument) IsBasic() bool {
	return a.BasicKind != 0
}

// IsAny returns true when argument is an empty interface or any.
//   interface{} or any.
func (a *Argument) IsAny() bool {
	return a.IsEmptyInterface
}

// IsMap returns true when argument is a Map.
//   map[string]*Type
func (a *Argument) IsMap() bool {
	return a.MapKey != nil && a.MapElem != nil
}

// Method method of the rpc service.
// Contains name of the method and arguments and result parameters.
type Method struct {
	Name         string    // Name for code generator (snake case of the GoName or special name from comments).
	GoName       string    // Name of the methos in the go source file.
	Comments     string    // Comments of the method from go source file.
	SwaggerTags  []string  // List of the tags in swagger spec.
	Parameter    *Argument // Parameter of the method.
	Result       *Argument // Result of the method.
	IsDeprecated bool      // Method marked as deprecated.
}

// Service is an interface in source code that describe rpc service.
// Interface contains methods and arguments with golang types.
type Service struct {
	Import    string    // Path of the package with interface.
	PkgName   string    // Name of the package with interface.
	Name      string    // Name for code generator (snake case of the GoName).
	GoName    string    // Name of the interface from go source file.
	Comments  string    // Comments of the service from go source file.
	Methods   []*Method // List of the methods.
	ErrorData *Argument // Model for "data" field in the JSON-RPC error.
}
