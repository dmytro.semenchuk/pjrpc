package analyzer_test

import (
	"go/ast"
	"strconv"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/analyzer"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

func TestJoinComments(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	doc := &ast.CommentGroup{
		List: []*ast.Comment{{Text: "// First line"}, {Text: "// Second line"}},
	}

	comment := &ast.CommentGroup{
		List: []*ast.Comment{{Text: "// Comment line"}},
	}

	testCase := func(want string, comments ...*ast.CommentGroup) {
		t.Helper()

		got := analyzer.JoinComments(comments...)

		so.Equal(want, got)
	}

	testCase("First line\nSecond line\nComment line\n", doc, comment)
	testCase("First line\nSecond line\n", doc)
	testCase("Comment line\n", comment)
	testCase("Comment line\n", nil, comment)
}

func TestParseJSONTag(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	const defaultFieldName = "Field"

	testCase := func(tag string, wantErr error, want *analyzer.FieldTag) {
		t.Helper()

		var fieldTag *ast.BasicLit

		if tag != "" {
			fieldTag = &ast.BasicLit{
				Value: tag,
			}
		}

		got, gotErr := analyzer.ParseStructTag(fieldTag, defaultFieldName)
		so.ErrorIs(gotErr, wantErr)
		so.Equal(want, got)
	}

	testCase(
		"",

		nil,
		&analyzer.FieldTag{
			Name: defaultFieldName,
		},
	)

	testCase(
		"`json:\"int,omitempty\" xml:\"int\"`",

		nil,
		&analyzer.FieldTag{
			Name:       "int",
			IsOptional: true,
		},
	)

	testCase(
		`json:"int,omitempty" xml:"int"`,

		nil,
		&analyzer.FieldTag{
			Name:       "int",
			IsOptional: true,
		},
	)

	testCase(
		"`json:\",omitempty\" xml:\"int\"`",

		nil,
		&analyzer.FieldTag{
			Name:       defaultFieldName,
			IsOptional: true,
		},
	)

	testCase(
		"`json:\"-\" xml:\"int\"`",

		nil,
		&analyzer.FieldTag{
			Name:      defaultFieldName,
			IsSkipped: true,
		},
	)

	testCase(
		"`json:\"\" xml:\"int\"`",

		nil,
		&analyzer.FieldTag{
			Name: defaultFieldName,
		},
	)

	testCase(
		"`xml:\"int\"`",

		nil,
		&analyzer.FieldTag{
			Name: defaultFieldName,
		},
	)

	testCase(
		"`json:\"int,omitempty\" genpjrpc:\"type:string\"`",

		nil,
		&analyzer.FieldTag{
			Name:        "int",
			IsOptional:  true,
			SwaggerKind: model.ArgumentKindString,
		},
	)

	testCase(
		"`json:\"int,omitempty\" genpjrpc:\"type:[]string\"`",

		analyzer.ErrInvalidData,
		nil,
	)

	testCase(
		"`json:\"int,omitempty\" genpjrpc:\"deprecated:true\"`",

		nil,
		&analyzer.FieldTag{
			Name:         "int",
			IsOptional:   true,
			IsDeprecated: true,
		},
	)

	testCase(
		"`json:\"int,omitempty\" genpjrpc:\"deprecated:ture\"`",

		strconv.ErrSyntax,
		nil,
	)

	testCase(
		"`json:\"int,omitempty\" genpjrpc:\"type:string,deprecated:true\"`",

		nil,
		&analyzer.FieldTag{
			Name:         "int",
			SwaggerKind:  model.ArgumentKindString,
			IsOptional:   true,
			IsDeprecated: true,
		},
	)

	testCase(
		"`json:\"int,omitempty\" genpjrpc:\"type:string deprecated:true\"`",

		analyzer.ErrInvalidData,
		nil,
	)

	testCase(
		"`json:\"int,omitempty\" genpjrpc:\"type:string,deprecated:true,custom:param\"`",

		analyzer.ErrInvalidData,
		nil,
	)
}

func TestParseMagicComments(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	testCase := func(doc *ast.CommentGroup, wantErr error, want *analyzer.MagicCommentParams) {
		t.Helper()

		got, gotErr := analyzer.ParseMagicComments(doc)
		so.ErrorIs(gotErr, wantErr)
		so.Equal(want, got)
	}

	testCase(
		nil,
		nil,
		new(analyzer.MagicCommentParams),
	)

	testCase(
		&ast.CommentGroup{List: []*ast.Comment{{Text: "// First line"}, {Text: "// Second line"}}},
		nil,
		new(analyzer.MagicCommentParams),
	)

	testCase(
		&ast.CommentGroup{List: []*ast.Comment{{Text: "// First line"}, {Text: "//genpjrpc:params method_name=name deprecated=true"}}},
		nil,
		&analyzer.MagicCommentParams{
			MethodName:   "name",
			IsDeprecated: true,
		},
	)

	testCase(
		&ast.CommentGroup{List: []*ast.Comment{{Text: "//genpjrpc:params method_name=name deprecated"}}},
		analyzer.ErrInvalidData,
		nil,
	)

	testCase(
		&ast.CommentGroup{List: []*ast.Comment{{Text: "//genpjrpc:params method_name=name deprecated=ture"}}},
		strconv.ErrSyntax,
		nil,
	)

	testCase(
		&ast.CommentGroup{List: []*ast.Comment{{Text: "//genpjrpc:params custome_param=value"}}},
		analyzer.ErrInvalidData,
		nil,
	)
}
