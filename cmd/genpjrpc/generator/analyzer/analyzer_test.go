package analyzer_test

import (
	"bytes"
	"context"
	"path/filepath"
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"
	"gitlab.com/so_literate/gentools/logger"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/config"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/analyzer"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

const (
	pkgTestdata = "gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/testdata/service"
	pkgCommon   = "gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/testdata/service/common"
)

func getLogger() (*bytes.Buffer, *zerolog.Logger) {
	buf := bytes.NewBuffer([]byte("\n"))

	conf := &logger.Config{
		Debug:   true,
		Pretty:  true,
		NoColor: true,
		Writer:  buf,
	}

	log := logger.New(conf)

	return buf, log
}

var parsedInType = &model.Argument{
	NamedName:    "In",
	NamedImport:  pkgTestdata,
	NamedComment: "In docs.\n",
	NamedObject: &model.Argument{
		StructFields: []*model.Argument{
			{
				StructFieldName:       "Int",
				StructFieldGoName:     "Int",
				StructFieldComment:    "Field comment.\n",
				StructFieldIsOptional: false,
				BasicKind:             model.ArgumentKindInt,
			},
			{
				StructFieldName:       "UUID",
				StructFieldGoName:     "UUID",
				StructFieldComment:    "Google uuid.\n",
				StructFieldIsOptional: false,
				BasicKind:             model.ArgumentKindString,
				BasicFormat:           model.ArgumentFormatUUID,
			},
			{
				StructFieldName:       "Decimal",
				StructFieldGoName:     "Decimal",
				StructFieldComment:    "Decimal.\n",
				StructFieldIsOptional: false,
				BasicKind:             model.ArgumentKindInt64,
				BasicFormat:           model.ArgumentFormat("integer"),
			},
			{
				StructFieldName:       "IEnum",
				StructFieldGoName:     "IEnum",
				StructFieldComment:    "Field enum comment.\n",
				StructFieldIsOptional: false,
				NamedImport:           pkgTestdata,
				NamedName:             "IntEnum",
				NamedComment:          "IntEnum enum of ints.\n",
				NamedObject: &model.Argument{
					EnumBasicKind: model.ArgumentKindInt,
					EnumElements: []*model.Argument{
						{
							EnumElementName:    "IntEnumFirst",
							EnumElementValue:   "1",
							EnumElementComment: "It's 1.\n",
						},
						{
							EnumElementName:    "IntEnumSecond",
							EnumElementValue:   "2",
							EnumElementComment: "It's 2.\n",
						},
						{
							EnumElementName:    "IntEnumThird",
							EnumElementValue:   "3",
							EnumElementComment: "It's 3.\n",
						},
					},
				},
			},
			{
				StructFieldName:       "SEnum",
				StructFieldGoName:     "SEnum",
				StructFieldComment:    "Enum field.\n",
				StructFieldIsOptional: false,
				NamedImport:           pkgTestdata,
				NamedName:             "StringEnum",
				NamedComment:          "StringEnum enum of strings.\n",
				NamedObject: &model.Argument{
					EnumBasicKind: model.ArgumentKindString,
					EnumElements: []*model.Argument{
						{
							EnumElementName:    "StringEnumFirst",
							EnumElementValue:   "FIRST",
							EnumElementComment: "It's first.\n",
						},
						{
							EnumElementName:    "StringEnumSecond",
							EnumElementValue:   "SECOND",
							EnumElementComment: "It's second.\n",
						},
						{
							EnumElementName:    "StringEnumThird",
							EnumElementValue:   "THIRD",
							EnumElementComment: "It's third.\n",
						},
					},
				},
			},
			{
				StructFieldName:       "Interface",
				StructFieldGoName:     "Interface",
				StructFieldComment:    "It is 'any' in 1.18.\n",
				StructFieldIsOptional: false,
				IsEmptyInterface:      true,
			},
			{
				StructFieldName:    "Time",
				StructFieldGoName:  "Time",
				StructFieldComment: "Will be RFC3339 string.\n",
				BasicKind:          model.ArgumentKindString,
				BasicFormat:        model.ArgumentFormatTime,
			},
		},
	},
}

var parsedOutType = &model.Argument{
	NamedName:    "Out",
	NamedImport:  pkgTestdata,
	NamedComment: "Out docs.\n",
	NamedObject: &model.Argument{
		StructFields: []*model.Argument{
			{
				StructFieldName:       "int",
				StructFieldGoName:     "Int",
				StructFieldComment:    "Field doc.\nField comment.\n",
				StructFieldIsOptional: true,
				BasicKind:             model.ArgumentKindInt,
			},
			{
				StructFieldName:       "ins",
				StructFieldGoName:     "Ins",
				StructFieldComment:    "",
				StructFieldIsOptional: false,
				SliceElement: &model.Argument{
					PointerElement: parsedInType,
				},
			},
			{
				StructFieldName:       "GoName",
				StructFieldGoName:     "GoName",
				StructFieldComment:    "",
				StructFieldIsOptional: true,
				BasicKind:             model.ArgumentKindString,
			},
			{
				StructFieldName:       "Field",
				StructFieldGoName:     "Field",
				StructFieldComment:    "",
				StructFieldIsOptional: false,
				BasicKind:             model.ArgumentKindString,
			},
			{
				StructFieldName:         "swagger_type",
				StructFieldGoName:       "SwaggerType",
				StructFieldComment:      "",
				StructFieldIsOptional:   true,
				StructFieldIsDeprecated: true,
				BasicKind:               model.ArgumentKindString,
			},
			{
				StructFieldName:       "map",
				StructFieldGoName:     "Map",
				StructFieldComment:    "",
				StructFieldIsOptional: false,
				MapKey: &model.Argument{
					BasicKind: model.ArgumentKindString,
				},
				MapElem: &model.Argument{
					IsEmptyInterface: true,
				},
			},
		},
	},
}

var parsedErrorDataType = &model.Argument{
	NamedName:    "ErrorData",
	NamedImport:  pkgTestdata,
	NamedComment: "ErrorData type for field \"data\" in the Error.\n",
	NamedObject: &model.Argument{
		StructFields: []*model.Argument{
			{
				StructFieldName:   "SomeMetaData",
				StructFieldGoName: "SomeMetaData",
				BasicKind:         model.ArgumentKindString,
			},
		},
	},
}

var expectedMethods = []*model.Method{
	{
		Name:        "special_method_name",
		GoName:      "SpecialMethod",
		Comments:    "Method comments.\n",
		SwaggerTags: []string{"FirstTag", "SecondTag"},
		Parameter:   parsedInType,
		Result:      parsedOutType,
	},
	{
		Name:         "deprecated_method",
		GoName:       "DeprecatedMethod",
		Comments:     "DeprecatedMethod method with mark deprecated.\n",
		Parameter:    parsedInType,
		Result:       parsedOutType,
		IsDeprecated: true,
	},
	{
		Name:     "slices",
		GoName:   "Slices",
		Comments: "",
		Parameter: &model.Argument{
			NamedName:    "SliceInt",
			NamedImport:  pkgTestdata,
			NamedComment: "SliceInt docs.\n",
			NamedObject: &model.Argument{
				SliceElement: &model.Argument{
					BasicKind: model.ArgumentKindInt,
				},
			},
		},
		Result: &model.Argument{
			NamedName:    "SliceStruct",
			NamedImport:  pkgTestdata,
			NamedComment: "SliceStruct docs.\n",
			NamedObject: &model.Argument{
				SliceElement: &model.Argument{
					PointerElement: parsedInType,
				},
			},
		},
	},
	{
		Name:     "basic",
		GoName:   "Basic",
		Comments: "",
		Parameter: &model.Argument{
			NamedName:    "AliasInt",
			NamedImport:  pkgTestdata,
			NamedComment: "AliasInt docs.\n",
			NamedObject: &model.Argument{
				BasicKind: model.ArgumentKindInt,
			},
		},
		Result: &model.Argument{
			NamedName:    "AliasString",
			NamedImport:  pkgTestdata,
			NamedComment: "AliasString docs.\n",
			NamedObject: &model.Argument{
				BasicKind: model.ArgumentKindString,
			},
		},
	},
	{
		Name:     "map",
		GoName:   "Map",
		Comments: "",
		Parameter: &model.Argument{
			NamedName:    "Dictionary",
			NamedImport:  pkgTestdata,
			NamedComment: "Dictionary just Go map.\n",
			NamedObject: &model.Argument{
				MapKey: &model.Argument{
					NamedImport:  pkgTestdata,
					NamedName:    "DictionaryEnum",
					NamedComment: "DictionaryEnum keys of the map.\n",
					NamedObject: &model.Argument{
						EnumBasicKind: model.ArgumentKindString,
						EnumElements: []*model.Argument{
							{
								EnumElementName:  "DictionaryEnumFirst",
								EnumElementValue: "first",
							},
							{
								EnumElementName:  "DictionaryEnumSecond",
								EnumElementValue: "second",
							},
						},
					},
				},
				MapElem: &model.Argument{
					PointerElement: parsedInType,
				},
			},
		},
		Result: parsedOutType,
	},
	{
		Name:     "from_external_package",
		GoName:   "FromExternalPackage",
		Comments: "",
		Parameter: &model.Argument{
			NamedName:    "Type",
			NamedImport:  pkgCommon,
			NamedComment: "Type is a type from common package.\n",
			NamedObject: &model.Argument{
				StructFields: []*model.Argument{
					{
						StructFieldName:       "Field",
						StructFieldGoName:     "Field",
						StructFieldComment:    "Field comment.\n",
						StructFieldIsOptional: false,
						BasicKind:             model.ArgumentKindString,
					},
				},
			},
		},
		Result: &model.Argument{
			NamedName:    "EmptyType",
			NamedImport:  pkgCommon,
			NamedComment: "EmptyType just empty no fields here.\n",
			NamedObject: &model.Argument{
				StructFields: []*model.Argument{},
			},
		},
	},
	{
		Name:     "secondary_use_type",
		GoName:   "SecondaryUseType",
		Comments: "",
		Parameter: &model.Argument{
			NamedName:   "ReqType",
			NamedImport: pkgTestdata,
			NamedObject: &model.Argument{
				StructFields: []*model.Argument{
					{
						StructFieldName:   "ReuseTime",
						StructFieldGoName: "ReuseTime",
						PointerElement: &model.Argument{
							NamedImport: pkgCommon,
							NamedName:   "TimeFilter",
							NamedObject: &model.Argument{
								StructFields: []*model.Argument{
									{
										StructFieldName:       "from",
										StructFieldGoName:     "From",
										StructFieldIsOptional: true,
										BasicKind:             model.ArgumentKindString,
										BasicFormat:           model.ArgumentFormatTime,
									},
									{
										StructFieldName:       "to",
										StructFieldGoName:     "To",
										StructFieldIsOptional: true,
										BasicKind:             model.ArgumentKindString,
										BasicFormat:           model.ArgumentFormatTime,
									},
								},
							},
						},
					},
					{
						StructFieldName:   "FirstTime",
						StructFieldGoName: "FirstTime",
						PointerElement: &model.Argument{
							NamedImport: pkgCommon,
							NamedName:   "ReusedType",
							NamedObject: &model.Argument{
								StructFields: []*model.Argument{
									{
										StructFieldName:   "Field",
										StructFieldGoName: "Field",
										BasicKind:         model.ArgumentKindInt64,
									},
								},
							},
						},
					},
				},
			},
		},
		Result: &model.Argument{
			NamedName:   "RespType",
			NamedImport: pkgTestdata,
			NamedObject: &model.Argument{
				StructFields: []*model.Argument{
					{
						StructFieldName:   "SecondTime",
						StructFieldGoName: "SecondTime",
						PointerElement: &model.Argument{
							NamedImport: pkgCommon,
							NamedName:   "ReusedType",
							NamedObject: &model.Argument{
								StructFields: []*model.Argument{
									{
										StructFieldName:   "Field",
										StructFieldGoName: "Field",
										BasicKind:         model.ArgumentKindInt64,
									},
								},
							},
						},
					},
				},
			},
		},
	},
}

func TestRun(t *testing.T) {
	t.Parallel()
	so := require.New(t)

	buf, log := getLogger()
	conf := &config.Search{
		Name:           "Service",
		Path:           filepath.Join("..", "..", "testdata", "service"),
		KnownTypesPath: filepath.Join("..", "..", "testdata", "service", "known_types.json"),
	}

	a, err := analyzer.New(log, conf)
	so.NoError(err)

	res, err := a.Run(context.Background())
	t.Log(buf.String())

	so.NoError(err)
	so.NotNil(res)

	so.Equal(pkgTestdata, res.Import)
	so.Equal("testdata", res.PkgName)
	so.Equal("service", res.Name)
	so.Equal("Service", res.GoName)
	so.Equal("Service desctiption comments.\nSecond line of the comments.\n", res.Comments)
	so.Equal(parsedErrorDataType, res.ErrorData)
	so.Equal(len(expectedMethods), len(res.Methods))

	for i := range res.Methods {
		so.Equalf(expectedMethods[i], res.Methods[i],
			"index %d - want %s | got %s",
			i, expectedMethods[i].GoName, res.Methods[i].GoName)
	}
}
