package analyzer

import (
	"errors"
	"fmt"
	"go/ast"
	"go/types"
	"strings"

	"github.com/iancoleman/strcase"
	"golang.org/x/tools/go/packages"

	"gitlab.com/so_literate/gentools/packager"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

const (
	magicNameMethodSetErrorData = "setErrorData"
)

func buildKnownTypeName(obj *types.TypeName) string {
	return fmt.Sprintf("%s.%s", obj.Pkg().Path(), obj.Name())
}

func (a *Analyzer) scanArgumentPointer(arg *types.Pointer) (*model.Argument, error) {
	elem, err := a.scanArgument(arg.Elem(), nil)
	if err != nil {
		return nil, fmt.Errorf("scanArgument: %w", err)
	}

	res := &model.Argument{
		PointerElement: elem,
	}

	return res, nil
}

func (a *Analyzer) scanArgumentEnumElement(elem *packager.ConstValue) *model.Argument {
	return &model.Argument{
		EnumElementName:    elem.Type.Name(),
		EnumElementValue:   strings.Trim(elem.Type.Val().String(), `"`),
		EnumElementComment: JoinComments(elem.Spec.Doc, elem.Spec.Comment),
	}
}

func (a *Analyzer) scanArgumentEnum(arg *types.Named) (*model.Argument, error) {
	// Fast check for basic type in the underlying object.
	_, isBasic := arg.Obj().Type().Underlying().(*types.Basic)
	if !isBasic {
		return nil, fmt.Errorf("%w: is not a basic type of the object", ErrNotEnumType)
	}

	values := a.pkgr.FindConstValuesOfType(arg)
	if len(values) == 0 {
		return nil, fmt.Errorf("%w: without const values", ErrNotEnumType)
	}

	basicArgument, err := a.scanArgument(arg.Obj().Type().Underlying(), nil)
	if err != nil {
		return nil, fmt.Errorf("scanArgument basic arg: %w", err)
	}

	if !basicArgument.IsBasic() {
		return nil, fmt.Errorf("%w: parsed basic argument is not basic", ErrInvalidData)
	}

	res := &model.Argument{
		EnumBasicKind: basicArgument.BasicKind,
		EnumElements:  make([]*model.Argument, len(values)),
	}

	for i, value := range values {
		res.EnumElements[i] = a.scanArgumentEnumElement(value)
	}

	return res, nil
}

func (a *Analyzer) scanArgumentNamed(arg *types.Named) (*model.Argument, error) {
	obj := arg.Obj()

	fullName := buildKnownTypeName(obj)

	knownType, ok := knownTypes[fullName]
	if ok {
		return &knownType, nil
	}

	if !obj.Exported() {
		return nil, fmt.Errorf("%w: named type is not exported", ErrInvalidData)
	}

	res := &model.Argument{
		NamedImport: obj.Pkg().Path(),
		NamedName:   obj.Name(),
	}

	// Recursive protection.
	knownTypes[fullName] = *res

	defer func() {
		// Save the latest changes.
		knownTypes[fullName] = *res
	}()

	genDecl := a.pkgr.GetGenDeclOfType(res.NamedImport, res.NamedName)
	if genDecl == nil {
		return nil, fmt.Errorf("%w: generic declaration not found", ErrInvalidData)
	}

	res.NamedComment = genDecl.Doc.Text()

	var err error

	// Maybe it's enum argument.
	res.NamedObject, err = a.scanArgumentEnum(arg)
	if err == nil {
		return res, nil
	}

	if !errors.Is(err, ErrNotEnumType) {
		return nil, fmt.Errorf("scanArgumentEnum: %w", err)
	}

	// It's not enum type just unwrap named type.
	res.NamedObject, err = a.scanArgument(obj.Type().Underlying(), genDecl)
	if err != nil {
		return nil, fmt.Errorf("scanArgument: %w", err)
	}

	return res, nil
}

func (a *Analyzer) scanArgumentStructField(typeField *types.Var, astField *ast.Field) (*model.Argument, error) {
	parsedTag, err := ParseStructTag(astField.Tag, typeField.Name())
	if err != nil {
		return nil, fmt.Errorf("failed to ParseStructTag: %w", err)
	}

	if parsedTag.IsSkipped {
		return nil, ErrFieldSkippedByTag
	}

	var res *model.Argument

	if parsedTag.SwaggerKind != model.ArgumentKindInvalid {
		res = &model.Argument{
			BasicKind: parsedTag.SwaggerKind, // just imitate basic kind argument without scan.
		}
	} else {
		res, err = a.scanArgument(typeField.Type(), nil)
		if err != nil {
			return nil, fmt.Errorf("scanArgument: %w", err)
		}
	}

	res.StructFieldComment = JoinComments(astField.Doc, astField.Comment)
	res.StructFieldGoName = typeField.Name()
	res.StructFieldName = parsedTag.Name
	res.StructFieldIsOptional = parsedTag.IsOptional
	res.StructFieldIsDeprecated = parsedTag.IsDeprecated

	return res, nil
}

func (a *Analyzer) scanArgumentStruct(arg *types.Struct, namedGenDecl *ast.GenDecl) (*model.Argument, error) {
	if namedGenDecl == nil {
		return nil, fmt.Errorf("%w: can't to scan struct type without named GenDecl", ErrInvalidData)
	}

	// nolint:errcheck,forcetypeassert // This is should be a StructType.
	astStruct := namedGenDecl.Specs[0].(*ast.TypeSpec).Type.(*ast.StructType)

	if arg.NumFields() != astStruct.Fields.NumFields() {
		return nil, fmt.Errorf(
			"%w: NumFields of the types.Struct not equals to NumFields of ast.StructType (%d != %d)",
			ErrInvalidData, arg.NumFields(), astStruct.Fields.NumFields(),
		)
	}

	res := &model.Argument{
		StructFields: make([]*model.Argument, 0, arg.NumFields()),
	}

	for i := 0; i < arg.NumFields(); i++ {
		typesField := arg.Field(i)

		if !typesField.Exported() {
			continue
		}

		field, err := a.scanArgumentStructField(typesField, astStruct.Fields.List[i])
		if err != nil {
			if errors.Is(err, ErrFieldSkippedByTag) {
				continue
			}

			return nil, fmt.Errorf("scanArgumentStructField (%d - %s): %w", i+1, typesField.Name(), err)
		}

		res.StructFields = append(res.StructFields, field)
	}

	return res, nil
}

func (a *Analyzer) scanArgumentBasic(arg *types.Basic) (*model.Argument, error) {
	res := &model.Argument{
		BasicKind: model.TypesKindToArgumentKind[arg.Kind()],
	}

	if res.BasicKind == model.ArgumentKindInvalid {
		return nil, fmt.Errorf("%w: basic type is not supported %s", ErrInvalidData, arg.String())
	}

	return res, nil
}

func (a *Analyzer) scanArgumentSlice(arg *types.Slice) (*model.Argument, error) {
	elem, err := a.scanArgument(arg.Elem(), nil)
	if err != nil {
		return nil, fmt.Errorf("scanArgument: %w", err)
	}

	res := &model.Argument{
		SliceElement: elem,
	}

	return res, nil
}

func (a *Analyzer) scanArgumentInterface(arg *types.Interface) (*model.Argument, error) {
	if !arg.Empty() {
		return nil, fmt.Errorf("%w: interface must be empty", ErrInvalidData)
	}

	return &model.Argument{IsEmptyInterface: true}, nil
}

func isMapKeyValid(key *model.Argument) bool {
	if key.IsBasic() {
		return key.BasicKind == model.ArgumentKindString
	}

	if key.IsNamed() && key.NamedObject.IsEnum() {
		return key.NamedObject.EnumBasicKind == model.ArgumentKindString
	}

	return false
}

func (a *Analyzer) scanArgumentMap(arg *types.Map) (*model.Argument, error) {
	key, err := a.scanArgument(arg.Key(), nil)
	if err != nil {
		return nil, fmt.Errorf("scanArgument key: %w", err)
	}

	if !isMapKeyValid(key) {
		return nil, fmt.Errorf("%w: key of the map must be 'string' or string Enum", ErrInvalidData)
	}

	elem, err := a.scanArgument(arg.Elem(), nil)
	if err != nil {
		return nil, fmt.Errorf("scanArgument element: %w", err)
	}

	res := &model.Argument{
		MapKey:  key,
		MapElem: elem,
	}

	return res, nil
}

func (a *Analyzer) scanArgument(arg types.Type, namedGenDecl *ast.GenDecl) (res *model.Argument, err error) {
	switch tp := arg.(type) {
	case *types.Pointer:
		res, err = a.scanArgumentPointer(tp)
		if err != nil {
			return nil, fmt.Errorf("scanArgumentPointer: %w", err)
		}

	case *types.Named:
		res, err = a.scanArgumentNamed(tp)
		if err != nil {
			return nil, fmt.Errorf("scanArgumentNamed (%q): %w", buildKnownTypeName(tp.Obj()), err)
		}

	case *types.Struct:
		res, err = a.scanArgumentStruct(tp, namedGenDecl)
		if err != nil {
			return nil, fmt.Errorf("scanArgumentStruct: %w", err)
		}

	case *types.Basic:
		res, err = a.scanArgumentBasic(tp)
		if err != nil {
			return nil, fmt.Errorf("scanArgumentBasic: %w", err)
		}

	case *types.Slice:
		res, err = a.scanArgumentSlice(tp)
		if err != nil {
			return nil, fmt.Errorf("scanArgumentSlice: %w", err)
		}

	case *types.Interface:
		res, err = a.scanArgumentInterface(tp)
		if err != nil {
			return nil, fmt.Errorf("scanArgumentInterface: %w", err)
		}

	case *types.Map:
		res, err = a.scanArgumentMap(tp)
		if err != nil {
			return nil, fmt.Errorf("scanArgumentMap: %w", err)
		}

	default:
		return nil, fmt.Errorf("%w: unsupported type: %T", ErrInvalidData, arg)
	}

	return res, nil
}

func (a *Analyzer) scanMethodArguments(args *types.Tuple) (*model.Argument, error) {
	if args == nil {
		return nil, fmt.Errorf("%w: arguments are nil", ErrInvalidData)
	}

	if l := args.Len(); l != 1 {
		return nil, fmt.Errorf("%w: wrong number of the argument: want 1, got %d", ErrInvalidData, l)
	}

	arg := args.At(0)

	res, err := a.scanArgument(arg.Type(), nil)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (a *Analyzer) scanMethod(method *ast.Field) (*model.Method, error) {
	if len(method.Names) != 1 {
		return nil, fmt.Errorf("%w: method names (%v) want 1, got %d", ErrInvalidData, method.Names, len(method.Names))
	}

	name := method.Names[0].Name

	res := &model.Method{
		Name:      strcase.ToSnake(name),
		GoName:    name,
		Comments:  method.Doc.Text(),
		Parameter: nil,
		Result:    nil,
	}

	commentParams, err := ParseMagicComments(method.Doc)
	if err != nil {
		return nil, fmt.Errorf("failed to parse magic comments: %q %w", name, err)
	}

	res.IsDeprecated = commentParams.IsDeprecated
	res.SwaggerTags = commentParams.SwaggerTags

	if commentParams.MethodName != "" {
		res.Name = commentParams.MethodName
	}

	// Convert method to types abstract to get more comfortable abstraction.
	methodTypes := a.pkgr.GetTypeOfExpr(method.Type)
	if methodTypes == nil {
		return nil, fmt.Errorf("%w: method type not found", ErrInvalidData)
	}

	sig, ok := methodTypes.(*types.Signature)
	if !ok {
		return nil, fmt.Errorf("%w (%q): method is not a Signature", ErrInvalidData, name)
	}

	if sig.Variadic() {
		return nil, fmt.Errorf("%w (%q): method contains variadic params", ErrInvalidData, name)
	}

	if res.Parameter, err = a.scanMethodArguments(sig.Params()); err != nil {
		return nil, fmt.Errorf("scanMethodArguments (params, %q): %w", name, err)
	}

	if res.GoName == magicNameMethodSetErrorData {
		return res, ErrMagicMethodSetErrorData
	}

	if res.Result, err = a.scanMethodArguments(sig.Results()); err != nil {
		return nil, fmt.Errorf("scanMethodArguments (results, %q): %w", name, err)
	}

	return res, nil
}

func (a *Analyzer) scanInterface(typeName *types.TypeName) (*model.Service, error) {
	// Need to scan ast interface with unordered list of the methods.
	// And we want to see comments of the declaration.
	// So we try to found ast interface and its generic declaration.
	ifaceAst := a.pkgr.GetGenDeclOfType(typeName.Pkg().Path(), typeName.Name())
	if ifaceAst == nil {
		return nil, fmt.Errorf("%w: ast of the interface not found", ErrInvalidData)
	}

	res := &model.Service{
		Import:   typeName.Pkg().Path(),
		PkgName:  typeName.Pkg().Name(),
		Name:     strcase.ToSnake(typeName.Name()),
		GoName:   typeName.Name(),
		Comments: ifaceAst.Doc.Text(),
	}

	// nolint:errcheck,forcetypeassert // This is must be interface.
	ifaceType := ifaceAst.Specs[0].(*ast.TypeSpec).Type.(*ast.InterfaceType)

	res.Methods = make([]*model.Method, 0, len(ifaceType.Methods.List))

	for i, m := range ifaceType.Methods.List {
		method, err := a.scanMethod(m)
		if err != nil {
			if errors.Is(err, ErrMagicMethodSetErrorData) {
				res.ErrorData = method.Parameter
				continue
			}

			return nil, fmt.Errorf("scanMethod [%d]: %w", i+1, err)
		}

		res.Methods = append(res.Methods, method)
	}

	return res, nil
}

func (a *Analyzer) scanPackage(pkg *packages.Package) (*model.Service, error) {
	obj := pkg.Types.Scope().Lookup(a.conf.Name)
	if obj == nil {
		return nil, fmt.Errorf("%w: not found by name %q", ErrProblemWithService, a.conf.Name)
	}

	if !types.IsInterface(obj.Type()) {
		return nil, fmt.Errorf("%w: is not an interface (name %q)", ErrProblemWithService, a.conf.Name)
	}

	// var ErrCustomeError = errors.New("error") - is an interface too but not a type statement.
	typeName, isTypeName := obj.(*types.TypeName)
	if !isTypeName {
		return nil, fmt.Errorf("%w: is not a TypeName (%T)", ErrProblemWithService, obj)
	}

	a.log.Debug().Msgf("scan the interface: %s", typeName.Name())

	service, err := a.scanInterface(typeName)
	if err != nil {
		return nil, fmt.Errorf("scanInterface %q: %w", typeName.Name(), err)
	}

	return service, nil
}
