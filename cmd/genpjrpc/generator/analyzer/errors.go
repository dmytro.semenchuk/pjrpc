package analyzer

import (
	"errors"
)

var (
	// ErrGoSyntax returns when parsed packages contains errors.
	ErrGoSyntax = errors.New("go syntax")
	// ErrWrongNumberPackages returns when analyzer got unexpected number of packeges.
	ErrWrongNumberPackages = errors.New("wrong number of packages")
	// ErrProblemWithService returns when package not contains type with service methods or has some problems.
	ErrProblemWithService = errors.New("problem with service type")
	// ErrInvalidData generic error returns when we got unexpected data.
	ErrInvalidData = errors.New("invalid data")
	// ErrFieldSkippedByTag field should be skipped in json parser.
	ErrFieldSkippedByTag = errors.New("field skipped by tag")
	// ErrNotEnumType named type is not an enum type.
	ErrNotEnumType = errors.New("not enum type")
	// ErrMagicMethodSetErrorData error for magic method to set error data model.
	ErrMagicMethodSetErrorData = errors.New("magic method setErrorData")
)
