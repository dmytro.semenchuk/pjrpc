package analyzer

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator/model"
)

// KnownTypes it is known types with Go basic type and format.
var knownTypes = map[string]model.Argument{
	// time.Time can be parsed with RFC3339 format.
	"time.Time": {
		BasicKind:   model.ArgumentKindString,
		BasicFormat: model.ArgumentFormatTime,
	},

	// google UUID type has JSON marshal to string.
	"github.com/google/uuid.UUID": {
		BasicKind:   model.ArgumentKindString,
		BasicFormat: model.ArgumentFormatUUID,
	},

	// decimal.Decimal has default JSON marshal to string.
	"github.com/shopspring/decimal.Decimal": {
		BasicKind: model.ArgumentKindString,
	},
}

// KnownType describes type with data of the basic type and optional format.
type KnownType struct {
	BasicType string `json:"basic_type"`
	Format    string `json:"format,omitempty"`
}

// KnownTypesFile describes file with known types.
// map["package.Type"] => {BasicType: "string", Format: "date-time"}.
type KnownTypesFile map[string]KnownType

// ParseKnownTypes parses known types from file.
func ParseKnownTypes(path string) (map[string]model.Argument, error) {
	if path == "" {
		return map[string]model.Argument{}, nil
	}

	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("os.Open: %w", err)
	}

	defer file.Close() // nolint:errcheck // don't care about this error.

	var parsed KnownTypesFile

	err = json.NewDecoder(file).Decode(&parsed)
	if err != nil {
		return nil, fmt.Errorf("json.Decode file (%s): %w", path, err)
	}

	result := make(map[string]model.Argument, len(parsed))

	for tp, descr := range parsed {
		kind := model.GetArgumentKind(descr.BasicType)
		if kind == model.ArgumentKindInvalid {
			return nil, fmt.Errorf("%w: unknown basic type: %q", ErrInvalidData, descr.BasicType)
		}

		result[tp] = model.Argument{
			BasicKind:   kind,
			BasicFormat: model.ArgumentFormat(descr.Format),
		}
	}

	return result, nil
}

func mergeKnownTypes(src, dst map[string]model.Argument) {
	for key, val := range src { // nolint:gocritic // it's ok to copy this data.
		dst[key] = val
	}
}
