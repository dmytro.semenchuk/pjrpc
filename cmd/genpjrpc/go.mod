module gitlab.com/pjrpc/pjrpc/cmd/genpjrpc

go 1.17

require (
	github.com/iancoleman/strcase v0.2.0
	github.com/rs/zerolog v1.25.0
	github.com/stretchr/testify v1.7.0
	gitlab.com/so_literate/fconfig v0.0.5
	gitlab.com/so_literate/gentools v0.0.3
	gitlab.com/so_literate/gentools/logger v0.0.1
	gitlab.com/so_literate/gentools/packager v0.0.5
	gitlab.com/so_literate/graceful v1.1.0
	gitlab.com/so_literate/openapi v0.0.3
	golang.org/x/tools v0.1.10
)

require (
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gitlab.com/so_literate/object v0.0.1 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220106191415-9b9b3d81d5e3 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	golang.org/x/xerrors v0.0.0-20220517211312-f3a8303e98df // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
