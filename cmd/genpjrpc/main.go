package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/so_literate/graceful"

	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/config"
	"gitlab.com/pjrpc/pjrpc/cmd/genpjrpc/generator"
)

// Run creates and runs pjrpc generator.
func Run(ctx context.Context, conf *config.Config, stdout io.Writer) error {
	gen, err := generator.New(conf, stdout)
	if err != nil {
		return fmt.Errorf("generator.New: %w", err)
	}

	if err = gen.Run(ctx); err != nil {
		return fmt.Errorf("generator.Run: %w", err)
	}

	return nil
}

func main() {
	log.SetFlags(0)

	run := func(ctx context.Context) error {
		conf, err := config.Load()
		if err != nil {
			return fmt.Errorf("config.Load: %w", err)
		}

		return Run(ctx, conf, os.Stdout)
	}

	graceful.TimeoutShutdown = time.Second

	if err := graceful.RunFuncs(run); err != nil {
		log.Println(err.Error())
		os.Exit(1)
	}
}
