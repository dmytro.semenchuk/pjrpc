package client_test

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"

	"gitlab.com/pjrpc/pjrpc/v2/client"
)

func ExampleClient() {
	cl, err := client.New("http://127.0.0.1:8080/rpc")
	if err != nil {
		return
	}

	type resultType struct {
		Field string `json:"field"`
	}

	var (
		ctx           = context.Background()
		idRequest     = "id_request"
		methodJSONRPC = "method_name"
		params        = "any json.Marshall(v) type"
		result        = new(resultType) // It have to be pointer type in the Invoke method (&result).
	)

	err = cl.Invoke(ctx, idRequest, methodJSONRPC, params, result)
	if err != nil {
		return
	}

	fmt.Printf("%#v", result)
}

func ExampleLogger() {
	cl, err := client.New("http://127.0.0.1:8080/rpc")
	if err != nil {
		return
	}

	// add your logger to the client.
	cl.SetLogger(log.Writer())
}

func ExampleMod() {
	customMod := func(req *http.Request) {
		dump, err := httputil.DumpRequestOut(req, true)
		if err != nil {
			return
		}
		fmt.Println(string(dump))
	}

	// Creates a client with custom mod that prints every request.
	cl, err := client.New("http://127.0.0.1:8080/rpc", customMod)
	if err != nil {
		return
	}

	var (
		ctx           = context.Background()
		idRequest     = "id_request"
		methodJSONRPC = "method_name"
		params        = "params"
		result        = ""
	)

	// Invokes method with an existing mod that sets custom header for this request.
	err = cl.Invoke(ctx, idRequest, methodJSONRPC, params, &result, client.ModWithHeader("X-Header", "value"))
	if err != nil {
		return
	}
}
