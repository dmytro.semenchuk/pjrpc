package client_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
	"gitlab.com/pjrpc/pjrpc/v2/client"
)

type testHTTPClient struct {
	req *http.Request

	resp *http.Response
	err  error
}

func (c *testHTTPClient) Do(req *http.Request) (*http.Response, error) {
	c.req = req
	return c.resp, c.err
}

func TestClientPositive(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	id := "id_req"
	method := "method_name"
	param := "param string"
	resultType := ""
	result := &resultType

	cl, err := client.New("http://endpoint", client.ModWithBasicAuth("user", "pwd"))
	if err != nil {
		t.Fatal("client.New:", err)
	}

	if cl.URL != "http://endpoint" {
		t.Fatal("cl.URL:", cl.URL)
	}

	if len(cl.Mods) != 1 {
		t.Fatal("wrong numbers of mods:", len(cl.Mods))
	}

	body := bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","result":"res string"}`))

	tcl := &testHTTPClient{
		req: nil,
		resp: &http.Response{
			StatusCode:    http.StatusOK,
			Header:        http.Header{pjrpc.ContentTypeHeaderName: []string{pjrpc.ContentTypeHeaderValue}},
			Body:          ioutil.NopCloser(body),
			ContentLength: int64(body.Len()),
		},
		err: nil,
	}

	cl.HTTPClient = tcl
	err = cl.Invoke(ctx, id, method, param, result, client.ModWithHeader("custom", "value"))
	if err != nil {
		t.Fatal("cl.Invoke:", err)
	}

	// Check mods.
	user, pwd, ok := tcl.req.BasicAuth()
	if user != "user" {
		t.Fatal("wrong user", user)
	}

	if pwd != "pwd" {
		t.Fatal("wrong pwd", pwd)
	}

	if !ok {
		t.Fatal("not ok")
	}

	if wantHeader := tcl.req.Header.Get("custom"); wantHeader != "value" {
		t.Fatal("wrong custom header:", wantHeader)
	}

	// Result was parsed correctly.
	if *result != "res string" {
		t.Fatal("wrong result:", *result)
	}

	gotBody, err := ioutil.ReadAll(tcl.req.Body)
	if err != nil {
		t.Fatal("ioutil.ReadAll body:", err)
	}

	wantBody := `{"jsonrpc":"2.0","id":"id_req","method":"method_name","params":"param string"}` + "\n"

	if string(gotBody) != wantBody {
		t.Fatalf("wrong body:\n%q\n%q", wantBody, string(gotBody))
	}

	//
	// error in response.
	//
	body = bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","error":{"code":123}}`))
	tcl.resp.Body = ioutil.NopCloser(body)
	err = cl.Invoke(ctx, id, method, param, &result)
	jErr := &pjrpc.ErrorResponse{}

	if !errors.As(err, &jErr) {
		t.Fatal("wrong error", err)
	}

	if jErr.Code != 123 {
		t.Fatal("wrong error code", jErr.Code)
	}
}

var (
	errDoerErorr = errors.New("doer error")

	errCloseErrBody = errors.New("error body")
)

type errBody struct{}

func (*errBody) Read(_ []byte) (n int, err error) {
	return 0, nil
}

func (*errBody) Close() error {
	return errCloseErrBody
}

func asserErrorIs(t *testing.T, got, want error) {
	t.Helper()

	if !errors.Is(got, want) {
		t.Fatalf("%v != %v", got, want)
	}
}

func TestClientNegative(t *testing.T) {
	t.Parallel()

	brokenURL := "http://end`point"
	validURL := "http://endpoint"
	ctx := context.Background()
	id := "id_req"
	method := "method_name"
	resultType := ""
	result := &resultType
	validParam := "valid_param"

	cl, err := client.New(brokenURL)
	urlErr := new(url.Error)
	if !errors.As(err, &urlErr) {
		t.Fatal("wrong error:", err)
	}

	if urlErr.Op != "parse" {
		t.Fatal("wrong err op:", urlErr.Op)
	}

	if cl != nil {
		t.Fatal("not empty client")
	}

	cl, err = client.New(validURL)
	if err != nil {
		t.Fatal("unexpected error:", err)
	}

	if cl == nil {
		t.Fatal("client is nil")
	}

	//
	// can't marshall type.
	//
	invalidParam := make(chan int)
	err = cl.Invoke(ctx, id, method, invalidParam, result)
	jsonErr := &json.UnsupportedTypeError{}
	if !errors.As(err, &jsonErr) {
		t.Fatal(err)
	}

	//
	// can't create request.
	//
	cl.URL = brokenURL
	err = cl.Invoke(ctx, id, method, validParam, result)
	if !errors.As(err, &urlErr) {
		t.Fatal(err)
	}

	//
	// common test client.
	tcl := new(testHTTPClient)
	cl.HTTPClient = tcl
	cl.URL = validURL

	//
	// error in http doer.
	//
	tcl.err = errDoerErorr
	err = cl.Invoke(ctx, id, method, validParam, result)
	asserErrorIs(t, err, errDoerErorr)

	//
	// bad status code.
	//
	tcl.err = nil
	tcl.resp = &http.Response{
		StatusCode: http.StatusBadRequest,
		Body:       ioutil.NopCloser(bytes.NewBuffer(nil)),
	}
	err = cl.Invoke(ctx, id, method, validParam, result)
	asserErrorIs(t, err, pjrpc.ErrBadStatusCode)

	//
	// don't have valid content type.
	//
	tcl.resp.StatusCode = http.StatusOK
	err = cl.Invoke(ctx, id, method, validParam, result)
	asserErrorIs(t, err, pjrpc.ErrWrongContentType)

	//
	// invalid body.
	//
	tcl.resp.Header = http.Header{pjrpc.ContentTypeHeaderName: []string{pjrpc.ContentTypeHeaderValue}}
	err = cl.Invoke(ctx, id, method, validParam, result)
	asserErrorIs(t, err, io.EOF)

	//
	// can't parse to invalid type.
	//
	body := bytes.NewBuffer([]byte(`{"jsonrpc":"2.0","id":"1","result":"res string"}`))
	tcl.resp.Body = ioutil.NopCloser(body)
	wrongResult := ""
	err = cl.Invoke(ctx, id, method, validParam, wrongResult)
	if !strings.Contains(err.Error(), "non-pointer") {
		t.Fatal(err)
	}

	//
	// Logging without logger.
	//
	tcl.resp.StatusCode = http.StatusInternalServerError
	tcl.resp.Body = &errBody{}
	err = cl.Invoke(ctx, id, method, validParam, result)
	if err == nil {
		t.Fatal("not empty error")
	}

	log := new(bytes.Buffer)
	cl.SetLogger(log)

	err = cl.Invoke(ctx, id, method, validParam, result)
	if err == nil {
		t.Fatal("not empty error")
	}

	wantMessage := "[pjrpc-client] (parseResponse): failed to close http response body: error body\n"
	if gotMessage := log.String(); wantMessage != gotMessage {
		t.Fatalf("unexpected message:\n%q\n%q", wantMessage, gotMessage)
	}
}
