package pjrpc_test

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/pjrpc/pjrpc/v2"
)

var errCustomError = errors.New("custom error")

type badReaderCloser struct{}

func (*badReaderCloser) Read(_ []byte) (n int, err error) {
	return 0, errCustomError
}

func (*badReaderCloser) Close() error {
	return errCustomError
}

type badResponseWriter struct{}

func (*badResponseWriter) Header() http.Header { return make(http.Header) }

func (*badResponseWriter) Write(_ []byte) (int, error) { return 0, errCustomError }

func (*badResponseWriter) WriteHeader(_ int) {}

func createRequest(t *testing.T, body string) (*httptest.ResponseRecorder, *http.Request) {
	t.Helper()

	req, err := http.NewRequestWithContext(
		context.Background(),
		http.MethodPost,
		"/rpc",
		strings.NewReader(body),
	)
	if err != nil {
		t.Fatalf("failed to create http request")
	}

	req.Header.Set("Content-Type", "application/json;charset=UTF-8")

	rr := httptest.NewRecorder()

	return rr, req
}

func assertProtocol(t *testing.T, rr *httptest.ResponseRecorder, wantID string) *pjrpc.Response {
	t.Helper()

	if rr.Code != http.StatusOK {
		t.Fatalf("wrong status code: %d", rr.Code)
	}

	gotCT := rr.Header().Get(pjrpc.ContentTypeHeaderName)

	if pjrpc.ContentTypeHeaderValue != gotCT {
		t.Fatal("wrong Content-Type header value:", gotCT)
	}

	resp := new(pjrpc.Response)
	if err := json.NewDecoder(rr.Body).Decode(resp); err != nil {
		t.Fatal("failed to decode response body:", err)
	}

	if pjrpc.JSONRPCVersion != resp.JSONRPC {
		t.Fatal("wrong JSON-RPC version:", resp.JSONRPC)
	}

	if gotID := resp.GetID(); wantID != gotID {
		t.Fatalf("wrong id: %q != %q", wantID, gotID)
	}

	rr.Flush()

	return resp
}

func assertResponseError(t *testing.T, wantErr, gotErr *pjrpc.ErrorResponse) {
	t.Helper()

	if wantErr == nil {
		if gotErr != nil {
			t.Fatalf("got unexpected error: %#v", gotErr)
		}

		return
	}

	if gotErr == nil {
		t.Fatalf("want error here: %#v", wantErr)
	}

	if wantErr.Code != gotErr.Code {
		t.Fatalf("wrong error code: %d != %d", wantErr.Code, gotErr.Code)
	}

	if wantErr.Message != gotErr.Message {
		t.Fatalf("wrong error message %q != %q", wantErr.Message, gotErr.Message)
	}

	if wantErr.Data == nil {
		if gotErr.Data != nil {
			t.Fatalf("got unexpected error data: %s", gotErr.Data)
		}

		return
	}

	assertRawMessages(t, wantErr.Data, gotErr.Data)
}

func assertRawMessages(t *testing.T, want, got json.RawMessage) {
	t.Helper()

	if !bytes.Equal(want, got) {
		t.Fatalf("wrong RawMessages:\n%q\n%q", want, got)
	}
}

func assertRawMessageWithString(t *testing.T, want string, got json.RawMessage) {
	t.Helper()

	assertRawMessages(t, json.RawMessage(want), got)
}

func TestServer(t *testing.T) {
	t.Parallel()

	srv := pjrpc.NewServerHTTP()

	//
	// parseRequest.
	//

	// request without header == invalid request.
	rr, req := createRequest(t, "")
	req.Header.Del("Content-Type")
	srv.ServeHTTP(rr, req)
	resp := assertProtocol(t, rr, "null")
	assertRawMessages(t, nil, resp.Result)
	assertResponseError(t, pjrpc.JRPCErrInvalidRequest("content-type must be 'application/json'"), resp.Error)

	// request without body == parse error.
	rr, req = createRequest(t, "")
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "null")
	assertRawMessages(t, nil, resp.Result)
	assertResponseError(t, pjrpc.JRPCErrInvalidRequest("body length is 0"), resp.Error)

	// request with wrong protocol == invalid request.
	rr, req = createRequest(t, `{"jsonrpc":"1.0"}`)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "null")
	assertRawMessages(t, nil, resp.Result)
	assertResponseError(t, pjrpc.JRPCErrInvalidRequest("wrong value of the field 'jsonrpc'"), resp.Error)

	// invalid body == invalid request.
	rr, req = createRequest(t, `{[]}`)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "null")
	assertRawMessages(t, nil, resp.Result)
	assertResponseError(t, pjrpc.JRPCErrInvalidRequest("failed to parse request"), resp.Error)

	// read from closed body.
	rr, req = createRequest(t, `{"jsonrpc":"2.0"}`)
	req.Body = new(badReaderCloser)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "null")
	assertRawMessages(t, nil, resp.Result)
	assertResponseError(t, pjrpc.JRPCErrParseError("failed to read body"), resp.Error)

	//
	// Method not found
	//
	// request without/wrong method name == method not found.
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"1","method":"method","params":{}}`)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "1")
	assertRawMessages(t, nil, resp.Result)
	assertResponseError(t, pjrpc.JRPCErrMethodNotFound(), resp.Error)

	//
	// Method with custom error.
	//
	srv.RegisterMethod("method", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		t.Helper()

		if string(params) != `{"field":"value"}` {
			t.Fatal("wrong params in 'method':", string(params))
		}

		return nil, errCustomError
	})
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"2","method":"method","params":{"field":"value"}}`)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "2")
	assertRawMessages(t, nil, resp.Result)
	assertResponseError(t, pjrpc.JRPCErrServerError(-32000, `method 'method': custom error`), resp.Error)

	//
	// Method with invalid type.
	//
	srv.RegisterMethod("invalid_type", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		return make(chan int), nil
	})
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"2.1","method":"invalid_type","params":{}}`)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "2.1")
	assertRawMessages(t, nil, resp.Result)
	assertResponseError(t, pjrpc.JRPCErrInternalError("failed to marshal response"), resp.Error)

	//
	// Method without error.
	//
	srv.RegisterMethod("method_2", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		t.Helper()

		if string(params) != `{"field2":"value2"}` {
			t.Fatal("wrong params in 'method_2':", string(params))
		}

		return "just text result", nil
	})
	wantTextResult := `"just text result"`

	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"3","method":"method_2","params":{"field2":"value2"}}`)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "3")
	assertResponseError(t, nil, resp.Error)
	assertRawMessageWithString(t, wantTextResult, resp.Result)

	//
	// With invalid content-length.
	//
	rr, req = createRequest(t, `{"jsonrpc":"2.0","id":"3.1","method":"method_2","params":{"field2":"value2"}}`)
	req.ContentLength = -1
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "3.1")
	assertResponseError(t, nil, resp.Error)
	assertRawMessageWithString(t, wantTextResult, resp.Result)

	//
	// Check panic in handler.
	//
	methodWithPanicBody := `{"jsonrpc":"2.0","id":"4","method":"method_with_panic","params":{}}`
	srv.RegisterMethod("method_with_panic", func(_ context.Context, _ json.RawMessage) (interface{}, error) {
		panic("test panic")
		return "method_with_panic result", nil // nolint:govet // 'unreachable: unreachable code', Yes it is
	})

	// With default panic handler.
	rr, req = createRequest(t, methodWithPanicBody)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "4")
	assertResponseError(t, pjrpc.JRPCErrInternalError(), resp.Error)

	// With custom panic handler, that just set crazy status code.
	srv.OnPanic = func(resp *pjrpc.Response, err error) {
		t.Helper()

		if !errors.Is(err, pjrpc.ErrPanicInHandler) {
			t.Fatalf("wrong error in OnPanic func:: %#v", err)
		}

		resp.Error = pjrpc.JRPCErrInvalidRequest()
	}

	rr, req = createRequest(t, methodWithPanicBody)
	srv.ServeHTTP(rr, req)
	resp = assertProtocol(t, rr, "4")
	assertResponseError(t, pjrpc.JRPCErrInvalidRequest(), resp.Error)

	// With Logger without panic handler.
	srv.OnPanic = nil
	log := bytes.NewBuffer(nil)
	srv.SetLogger(log)
	rr, req = createRequest(t, methodWithPanicBody)
	srv.ServeHTTP(rr, req)

	wantPanicMessage := "[pjrpc-server] (restorePanic): panic: panic in handler: 'method_with_panic': test panic\n"

	if !strings.Contains(log.String(), wantPanicMessage) {
		t.Fatal("wrong panic message:\n", log.String())
	}

	//
	// Without panic handlers just print in log.
	//
	srv.Logger = nil
	rr, req = createRequest(t, methodWithPanicBody)
	srv.ServeHTTP(rr, req)

	//
	// log error in writing response body.
	//
	log = bytes.NewBuffer(nil)
	srv.SetLogger(log)
	_, req = createRequest(t, `{"jsonrpc":"2.0","id":"5","method":"method_2","params":{"field2":"value2"}}`)
	srv.ServeHTTP(new(badResponseWriter), req)

	wantErrorMessage := "[pjrpc-server] (sendResponseBody): failed to write response: custom error\n"

	if wantErrorMessage != log.String() {
		t.Fatal("wrong error message:\n", log.String())
	}
}

func assertProtocolBatch(t *testing.T, rr *httptest.ResponseRecorder, ids ...string) pjrpc.BatchResponses {
	t.Helper()

	if http.StatusOK != rr.Code {
		t.Fatal("wrong status code:", rr.Code)
	}

	gotCT := rr.Header().Get(pjrpc.ContentTypeHeaderName)

	if pjrpc.ContentTypeHeaderValue != gotCT {
		t.Fatal("wrong Content-Type header value:", gotCT)
	}

	var respBatch pjrpc.BatchResponses

	if err := json.NewDecoder(rr.Body).Decode(&respBatch); err != nil {
		t.Fatal("failed to decode response body:", err)
	}

	if len(respBatch) != len(ids) {
		t.Fatalf("wrong number responses: %d != %d", len(respBatch), len(ids))
	}

	for i, resp := range respBatch {
		if pjrpc.JSONRPCVersion != resp.JSONRPC {
			t.Fatalf("wrong JSON-RPC version [%d]: %s", i, resp.JSONRPC)
		}

		gotID := resp.GetID()
		wantID := ids[i]

		if wantID != gotID {
			t.Fatalf("wrong id [%d]: %q != %q", i, wantID, gotID)
		}
	}

	rr.Flush()

	return respBatch
}

func TestServerBatch(t *testing.T) {
	t.Parallel()

	srv := pjrpc.NewServerHTTP()

	//
	// Single request in Batch.
	//
	srv.RegisterMethod("method", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		if string(params) == `{"single":"request"}` {
			return "result", nil
		}
		return "multi", nil
	})
	wantResponseResult := `"result"`
	wantResponseMulti := `"multi"`

	rr, req := createRequest(t, `[{"jsonrpc":"2.0","id":"1","method":"method","params":{"single":"request"}}]`)
	srv.ServeHTTP(rr, req)
	respBatch := assertProtocolBatch(t, rr, "1")

	assertResponseError(t, nil, respBatch[0].Error)
	assertRawMessageWithString(t, wantResponseResult, respBatch[0].Result)

	//
	// Many requests in Batch.
	//
	rr, req = createRequest(t, `[
		{"jsonrpc":"2.0","id":"1","method":"method","params":{"single":"request"}},
		{"jsonrpc":"2.0","id":"2","method":"method","params":{"single":"multi"}}
	]`)
	srv.ServeHTTP(rr, req)
	respBatch = assertProtocolBatch(t, rr, "1", "2")

	assertResponseError(t, nil, respBatch[0].Error)
	assertRawMessageWithString(t, wantResponseResult, respBatch[0].Result)
	assertResponseError(t, nil, respBatch[1].Error)
	assertRawMessageWithString(t, wantResponseMulti, respBatch[1].Result)

	//
	// Many requests with error Batch.
	//
	srv.RegisterMethod("error", func(ctx context.Context, params json.RawMessage) (interface{}, error) {
		return nil, pjrpc.JRPCErrInternalError("error")
	})
	rr, req = createRequest(t, `[
		{"jsonrpc":"2.0","id":"1","method":"method","params":{"single":"request"}},
		{"jsonrpc":"2.0","id":"2","method":"error","params":{"single":"multi"}}
	]`)
	srv.ServeHTTP(rr, req)
	respBatch = assertProtocolBatch(t, rr, "1", "2")

	assertResponseError(t, nil, respBatch[0].Error)
	assertRawMessageWithString(t, wantResponseResult, respBatch[0].Result)
	assertResponseError(t, pjrpc.JRPCErrInternalError("error"), respBatch[1].Error)
	assertRawMessages(t, nil, respBatch[1].Result)

	//
	// Batch request with wrong body.
	//
	rr, req = createRequest(t, `[[]]`)
	srv.ServeHTTP(rr, req)
	respBatch = assertProtocolBatch(t, rr, "null")
	assertResponseError(t, pjrpc.JRPCErrInvalidRequest("failed to parse request"), respBatch[0].Error)
	assertRawMessages(t, nil, respBatch[0].Result)

	//
	// Many requests with invalid request Batch.
	//
	rr, req = createRequest(t, `[{"jsonrpc":"1.0","id":"3"}]`)
	srv.ServeHTTP(rr, req)
	respBatch = assertProtocolBatch(t, rr, "3")

	assertResponseError(t, pjrpc.JRPCErrInvalidRequest("wrong value of the field 'jsonrpc'"), respBatch[0].Error)
	assertRawMessages(t, nil, respBatch[0].Result)
}
